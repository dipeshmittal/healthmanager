package com.health.manager;

import java.util.Calendar;

import com.health.manager.dal.GlucoReadingsDALManager;

public class GlucoReadingSchedule {
	
	private String uid;
	private String applyDate;
	private String endDate;
	private String isActive;
	
	private boolean fasting;
	private boolean postBreak;
	private boolean preLunch;
	private boolean postLunch;
	private boolean preDinner;
	private boolean postDinner;
	
	private boolean sunday;
	private boolean monday;
	private boolean tuesday;
	private boolean wednesday;
	private boolean thursday;
	private boolean friday;
	private boolean saturday;
	
	private String pattern;
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public boolean isFasting() {
		return fasting;
	}
	public void setFasting(boolean fasting) {
		this.fasting = fasting;
	}
	public boolean isPostBreak() {
		return postBreak;
	}
	public void setPostBreak(boolean postBreak) {
		this.postBreak = postBreak;
	}
	public boolean isPreLunch() {
		return preLunch;
	}
	public void setPreLunch(boolean preLunch) {
		this.preLunch = preLunch;
	}
	public boolean isPostLunch() {
		return postLunch;
	}
	public void setPostLunch(boolean postLunch) {
		this.postLunch = postLunch;
	}
	public boolean isPreDinner() {
		return preDinner;
	}
	public void setPreDinner(boolean preDinner) {
		this.preDinner = preDinner;
	}
	public boolean isPostDinner() {
		return postDinner;
	}
	public void setPostDinner(boolean postDinner) {
		this.postDinner = postDinner;
	}
	public boolean isSunday() {
		return sunday;
	}
	public void setSunday(boolean sunday) {
		this.sunday = sunday;
	}
	public boolean isMonday() {
		return monday;
	}
	public void setMonday(boolean monday) {
		this.monday = monday;
	}
	public boolean isTuesday() {
		return tuesday;
	}
	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}
	public boolean isWednesday() {
		return wednesday;
	}
	public void setWednesday(boolean wednesday) {
		this.wednesday = wednesday;
	}
	public boolean isThursday() {
		return thursday;
	}
	public void setThursday(boolean thursday) {
		this.thursday = thursday;
	}
	public boolean isFriday() {
		return friday;
	}
	public void setFriday(boolean friday) {
		this.friday = friday;
	}
	public boolean isSaturday() {
		return saturday;
	}
	public void setSaturday(boolean saturday) {
		this.saturday = saturday;
	}
	
	public String getPattern() {
		
		if (pattern == null){
			
			String dayPattern = string(this.fasting) + string(this.postBreak) + 
					string(this.postLunch) + string(this.postLunch) + 
					string(this.preDinner) + string(this.postDinner);
			
			String weekPattern = string(this.sunday, dayPattern) + string(this.monday, dayPattern) +
					string(this.tuesday, dayPattern) + string(this.wednesday, dayPattern) +
					string(this.thursday, dayPattern) + string(this.friday, dayPattern) +
					string(this.saturday, dayPattern);
					
			this.pattern = weekPattern;
		}

		System.out.println("Pattern : " + pattern);
		return pattern;
	}
	
	private void setWeekPattern(int day, boolean value){
		
		switch(day){
			case 0 : {
				this.sunday = value;
			}
			case 1 : {
				this.monday = value;
			}
			case 2 : {
				this.tuesday = value;
			}
			case 3 : {
				this.wednesday = value;
			}
			case 4 : {
				this.thursday = value;
			}
			case 5 : {
				this.friday = value;
			}
			case 6 : {
				this.saturday = value;
			}
		}
	}
	
	private void setDayPattern(String dayPattern){
		
		this.fasting = dayPattern.charAt(0) == '1';
		this.postBreak = dayPattern.charAt(1) == '1';
		this.preLunch = dayPattern.charAt(2) == '1';
		this.postLunch = dayPattern.charAt(3) == '1';
		this.preDinner = dayPattern.charAt(4) == '1';
		this.postDinner = dayPattern.charAt(5) == '1';
	}
	
	public void setPattern(String pattern) {
		this.pattern = pattern;
		
		this.fasting = false;
		this.postBreak = false;
		this.preLunch = false;
		this.postLunch = false;
		this.preDinner = false;
		this.postDinner = false;

		String[] weekPattern = new String[7];
		for (int i = 1; i <= 7; i++){
			weekPattern[i-1] = pattern.substring((i-1)*6, i*6);
		}
				
		for (int i = 0; i < 7; i++){
			if (weekPattern[i].equalsIgnoreCase("000000")){
				setWeekPattern(i, false);
			}
			else {
				setWeekPattern(i, true);
				setDayPattern(weekPattern[i]);
			}
		}
		
	}
	
	private String string(boolean value){
		
		return value ? "1" : "0";
		
	}
	
	private String string(boolean value, String dayPattern){
		
		return value ? dayPattern : "000000";
	}

}
