package com.health.manager;

public class UserHealthInfo {

	private String primaryUserId;
	private String doctorName;
	private String typeDiabetes;
	private String glucoDevice;
	private String dateOfDiagnosis;
	
	public String getPrimaryUserId() {
		return primaryUserId;
	}
	public void setPrimaryUserId(String primaryUserId) {
		this.primaryUserId = primaryUserId;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getTypeDiabetes() {
		return typeDiabetes;
	}
	public void setTypeDiabetes(String typeDiabetes) {
		this.typeDiabetes = typeDiabetes;
	}
	public String getGlucoDevice() {
		return glucoDevice;
	}
	public void setGlucoDevice(String glucoDevice) {
		this.glucoDevice = glucoDevice;
	}
	public String getDateOfDiagnosis() {
		return dateOfDiagnosis;
	}
	public void setDateOfDiagnosis(String dateOfDiagnosis) {
		this.dateOfDiagnosis = dateOfDiagnosis;
	}
	
}
