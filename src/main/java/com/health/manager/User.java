package com.health.manager;

import java.util.List;

public class User {

	private String uid;
	private String username;
	private String dateOfBirth;
	private String firstName;
	private String lastName;
	private String password;
	private String confirmPassword;
	private String email;
	private String gender;
	private String height;
	private String weight;
	private String bloodGroup;
	private String occupation;
	private String phoneNumber;
	private String alternatePhoneNumber;
	private String addressStreet;
	private String addressCity;
	private String addressPincode;
	private String signUpDate;
	private String type;
	
	private List<GlucoReading> fasting;
	private List<GlucoReading> preLunch;
	private List<GlucoReading> preDinner;
	
	private List<GlucoReading> postBreakFast;
	private List<GlucoReading> postLunch;
	private List<GlucoReading> postDinner;
	
	private List<GlucoReading> random;
	private List<GlucoReading> postMeds;
	
	private UserHealthInfo healthInfo;
	
	public String getAlternatePhoneNumber() {
		return alternatePhoneNumber;
	}
	public void setAlternatePhoneNumber(String alternatePhoneNumber) {
		this.alternatePhoneNumber = alternatePhoneNumber;
	}
	public String getAddressStreet() {
		return addressStreet;
	}
	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}
	public String getAddressCity() {
		return addressCity;
	}
	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}
	public String getAddressPincode() {
		return addressPincode;
	}
	public void setAddressPincode(String addressPincode) {
		this.addressPincode = addressPincode;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getSignUpDate() {
		return signUpDate;
	}
	public void setSignUpDate(String signUpDate) {
		this.signUpDate = signUpDate;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<GlucoReading> getFasting() {
		return fasting;
	}
	public void setFasting(List<GlucoReading> fasting) {
		this.fasting = fasting;
	}
	public List<GlucoReading> getPreLunch() {
		return preLunch;
	}
	public void setPreLunch(List<GlucoReading> preLunch) {
		this.preLunch = preLunch;
	}
	public List<GlucoReading> getPreDinner() {
		return preDinner;
	}
	public void setPreDinner(List<GlucoReading> preDinner) {
		this.preDinner = preDinner;
	}
	public List<GlucoReading> getPostBreakFast() {
		return postBreakFast;
	}
	public void setPostBreakFast(List<GlucoReading> postBreakFast) {
		this.postBreakFast = postBreakFast;
	}
	public List<GlucoReading> getPostLunch() {
		return postLunch;
	}
	public void setPostLunch(List<GlucoReading> postLunch) {
		this.postLunch = postLunch;
	}
	public List<GlucoReading> getPostDinner() {
		return postDinner;
	}
	public void setPostDinner(List<GlucoReading> postDinner) {
		this.postDinner = postDinner;
	}
	public List<GlucoReading> getRandom() {
		return random;
	}
	public void setRandom(List<GlucoReading> random) {
		this.random = random;
	}
	public List<GlucoReading> getPostMeds() {
		return postMeds;
	}
	public void setPostMeds(List<GlucoReading> postMeds) {
		this.postMeds = postMeds;
	}
	public UserHealthInfo getHealthInfo() {
		return healthInfo;
	}
	public void setHealthInfo(UserHealthInfo healthInfo) {
		this.healthInfo = healthInfo;
	}
	
	
	
}
