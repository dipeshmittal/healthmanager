package com.health.manager;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.web.multipart.MultipartFile;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxEntry.WithChildren;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuthNoRedirect;
import com.dropbox.core.DbxWriteMode;

public class DropBoxUtil {
	
	private static String userLocale = Locale.getDefault().toString();
	private static String token = "Ad6qqw-cQBgAAAAAAAAABrKBMElrLOGHEMKj31GSnlBW5suXdL38hAWbDeLNOzoX";
	private static DbxAppInfo appInfo = new DbxAppInfo("qtidkm8xnx7uhix", "ikl9wpdhpob6scg");
	private static DbxRequestConfig config = new DbxRequestConfig("DbxUploader", userLocale);
	
	public static void upLoadFileToDropBox(String uid, String purpose, 
			String name, String date, MultipartFile file){
		
		if (file == null){
			return;
		}
		
		DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);
		
		DbxClient dbxClient = new DbxClient(config, token);
		
		try {
			String originalName = file.getOriginalFilename();
			// System.out.println("Original Name: " + originalName);
			String[] tokens = originalName.split(".");
			
			String fileName = name != null ? name : originalName;
			
			System.out.println("File Name: " + fileName);
			dbxClient.uploadFile("/" + uid + "/" + purpose +"/" + date +"/" + fileName,
					DbxWriteMode.add(), file.getSize(), file.getInputStream());
			
		} catch (Exception e) {
			System.out.println("File not uploaded because of : " + e.getMessage());
		}
	}
	
	public static List<UserFile> getMetadata(String uid){
		
		if (uid == null){
			return null;
		}
		
		DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);
		
		DbxClient dbxClient = new DbxClient(config, token);
		
		List<UserFile> files = new ArrayList<UserFile>();
		
		try {
			
			System.out.println("UID: " + uid);
			WithChildren entry = dbxClient.getMetadataWithChildren("/" + uid);
			
			getFilesMetadata(dbxClient, entry.children, files);
			
		} catch (Exception e) {
			System.out.println("Metadata not found because of : " + e.getMessage());
		}
		
		return files;
	
	}
	
	public static void getFile(String path, String name){
		
		if (path == null){
			return;
		}
		
		String correctPath = path.replace(" ", "%20");
		HttpResponse response = null;
				
		HttpClient client = HttpClientBuilder.create().build();
		
		System.out.println("Corrected Path : " + correctPath);
		HttpGet get = new HttpGet("https://api-content.dropbox.com/1/files/auto" + correctPath);
		get.addHeader("Authorization", "Bearer " + token);
 
		String filePathVM = "/root/apache-tomcat-7.0.56/webapps/ROOT/resources/files/"+ name +".pdf";
		String filePathDev = "/Users/dimittal/githome/HealthManager/target/root/resources/files/"+ name +".pdf";
		
		String filePath = filePathVM;
		
		try {
			response = client.execute(get);
			System.out.println("Here : 0");
			File f = new File(filePath);
			
			if (f.exists()){
				f.delete();
				System.out.println("File deleted from : " + f.getAbsolutePath());
			}
			
			FileOutputStream fos = new FileOutputStream(filePath);
			System.out.println("Here : 1");
			//create an object of BufferedOutputStream
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			System.out.println("Here : 2");

			response.getEntity().writeTo(bos);
			System.out.println("Here : 3");
			bos.close();
			fos.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Here : 4");
		}
	
	}
	
	private static void getFilesMetadata(DbxClient dbxClient, List<DbxEntry> entries, List<UserFile> files) throws DbxException{
		if (entries != null){
			for (DbxEntry entry : entries){
				if (entry.isFolder()){
					WithChildren childEntry = dbxClient.getMetadataWithChildren(entry.path);
					
					getFilesMetadata(dbxClient, childEntry.children, files);
				}
				else {
					System.out.println(entry.path);
					String[] tokens = entry.path.split("/");
					
					UserFile file = new UserFile();
					file.setUid(tokens[1]);
					file.setPath(entry.path);
					file.setName(entry.name);
					file.setType(tokens[2]);
					file.setDate(tokens[3]);
					//file.setUrl(dbxClient.createShareableUrl(entry.path));

					files.add(file);
				}
			}
			
		}
	}
	
}
