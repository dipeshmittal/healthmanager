package com.health.manager.hibernate;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="User_Health_Info")
public class UserHealthInfoDO {

	@Id
	@Column(name="user_id")
    private Integer userId;
     
    @Column(name="doctor_name")
    private String doctorName;
     
    @Column(name="type_diabetes")
    private String typeDiabetes;
    
    @Column(name="gluco_device")
    private Integer glucoDevice;
    
    @Column(name="time_created")
    private Date timeCreated;
    
    @Column(name="diabetes_since")
    private Date diabetesSince;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getTypeDiabetes() {
		return typeDiabetes;
	}

	public void setTypeDiabetes(String typeDiabetes) {
		this.typeDiabetes = typeDiabetes;
	}

	public Integer getGlucoDevice() {
		return glucoDevice;
	}

	public void setGlucoDevice(Integer glucoDevice) {
		this.glucoDevice = glucoDevice;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getDiabetesSince() {
		return diabetesSince;
	}

	public void setDiabetesSince(Date diabetesSince) {
		this.diabetesSince = diabetesSince;
	}
	

}
