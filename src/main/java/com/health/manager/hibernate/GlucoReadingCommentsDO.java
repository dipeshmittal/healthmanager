package com.health.manager.hibernate;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Glucose_Reading_Comments")
public class GlucoReadingCommentsDO {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Integer id;
    
    @Column(name = "glucose_reading_id")
    private Integer glucoseReadingId;
    
    @Column(name = "time_commented")
    private Timestamp timeCommented;
    
    @Column(name = "type")
    private String type;
    
    @Column(name = "commented_by_user_id")
    private Integer commentedByUserId;
    
    @Column(name = "comment")
    private String comment;
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGlucoseReadingId() {
		return glucoseReadingId;
	}

	public void setGlucoseReadingId(Integer glucoseReadingId) {
		this.glucoseReadingId = glucoseReadingId;
	}

	public Timestamp getTimeCommented() {
		return timeCommented;
	}

	public void setTimeCommented(Timestamp timeCommented) {
		this.timeCommented = timeCommented;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getCommentedByUserId() {
		return commentedByUserId;
	}

	public void setCommentedByUserId(Integer commentedByUserId) {
		this.commentedByUserId = commentedByUserId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
