package com.health.manager.dal;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.health.manager.GlucoReading;
import com.health.manager.GlucoReadingSchedule;
import com.health.manager.User;
import com.health.manager.hibernate.GlucoReadingCommentsDO;
import com.health.manager.hibernate.GlucoReadingDO;
import com.health.manager.hibernate.GlucoReadingScheduleDO;
import com.health.manager.hibernate.HibernateUtil;
import com.health.manager.hibernate.UserDO;
import com.health.manager.hibernate.UserHealthInfoDO;

public class GlucoReadingsDALManager {

	public static List<GlucoReading> getLatestReadings(int num){
		
		List<?> readingList = getLatest(num, "P");
		
		List<GlucoReading> result = new ArrayList<GlucoReading>();
		
		for (int i = 0; i < readingList.size(); i++){
			GlucoReadingDO reading = (GlucoReadingDO) readingList.get(i);
			
			result.add(transformFromGlucoReadingDO(reading));
		}
		
		return result;
	}
	
	public static List<GlucoReading> getLatestFlaggedReadings(int num){
		
		List<?> readingList = getLatest(num, "F");
		
		List<GlucoReading> result = new ArrayList<GlucoReading>();
		
		for (int i = 0; i < readingList.size(); i++){
			GlucoReadingDO reading = (GlucoReadingDO) readingList.get(i);
			
			result.add(transformFromGlucoReadingDO(reading));
		}
		
		return result;
	}

	public static List<GlucoReading> getLatestReviewedReadings(int num){
		
		List<?> readingList = getLatest(num, "R");
		
		List<GlucoReading> result = new ArrayList<GlucoReading>();
		
		for (int i = 0; i < readingList.size(); i++){
			GlucoReadingDO reading = (GlucoReadingDO) readingList.get(i);
			
			result.add(transformFromGlucoReadingDO(reading));
		}
		
		return result;
	}
	
	public static void addCommentOnReading(GlucoReading readingComment, String type){
		
		GlucoReadingCommentsDO commentDO = new GlucoReadingCommentsDO();
		commentDO.setComment(readingComment.getComment());
		commentDO.setGlucoseReadingId(Integer.parseInt(readingComment.getId()));
		commentDO.setTimeCommented(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		commentDO.setType(type);
		
		int commentId = save(commentDO, type);
	}
	
	public static void addGlucoReadingSchedule(GlucoReadingSchedule readingSchedule){
		
		GlucoReadingScheduleDO scheduleDO = new GlucoReadingScheduleDO();
		scheduleDO.setTimeCreated(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		scheduleDO.setUserId(Integer.parseInt(readingSchedule.getUid()));
		scheduleDO.setPattern(readingSchedule.getPattern());
		scheduleDO.setStartDate(Date.valueOf(readingSchedule.getApplyDate()));
		scheduleDO.setStatus("A");
		
		int scheduleId = save(scheduleDO);
	}
	
	public static List<GlucoReadingSchedule> getAllVitalsSchedule(String uid){
		Integer id = Integer.parseInt(uid);
		
		List<?> list = getAllVitalsSchedule(id);
		
		if (list == null || list.isEmpty()){
			return null;
		}
		
		List<GlucoReadingSchedule> scheduleList = new ArrayList<GlucoReadingSchedule>();
		
		for (int i = 0; i < list.size(); i++){
			GlucoReadingScheduleDO scheduleDO = (GlucoReadingScheduleDO) list.get(i);
			
			GlucoReadingSchedule schedule = new GlucoReadingSchedule();
			schedule.setApplyDate(scheduleDO.getStartDate().toString());
			schedule.setPattern(scheduleDO.getPattern());
			
			if (scheduleDO.getEndDate() != null){
				schedule.setEndDate(scheduleDO.getEndDate().toString());
			}
			
			schedule.setIsActive(scheduleDO.getStatus());
			
			scheduleList.add(schedule);
			
		}
		
		return scheduleList;
	}
	
	public static String getVitalsSchedule(String uid){
		
		Integer id = Integer.parseInt(uid);
		Calendar cal = Calendar.getInstance();
		
		GlucoReadingScheduleDO schedule = getActiveVitalsSchedule(id);
		if (schedule != null){
			
			if (schedule.getEndDate() == null){
				// The active schedule is the only schedule
				System.out.println("Schedule is active and alone");
				return schedule.getPattern();
			}
			else {
				if (schedule.getEndDate().compareTo(cal.getTime()) >= 0){
					// The active schedule is not the only one, but is still applicable
					System.out.println("Schedule is active and applicable");
					return schedule.getPattern();
				} 
				else {
					// The active schedule is not the only one, and is not applicable
					// We need to make it inactive and search for the pending one.
					schedule.setStatus("I");
					
					GlucoReadingScheduleDO newSchedule = getPendingVitalsSchedules(id);
					newSchedule.setStatus("A");
					
					update(schedule, newSchedule);
					System.out.println("Schedule is pending");
					return newSchedule.getPattern();
				}
			}
		}
		
		System.out.println("Schedule is null, no active ones found");
		return null;
	}
	
	public static void addGlucoReading(GlucoReading reading){
		
		GlucoReadingDO readingDO = new GlucoReadingDO();
		UserDO user = new UserDO();
		user.setId(Integer.parseInt(reading.getUid()));
		readingDO.setUser(user);
		readingDO.setDeviceId(1);
		readingDO.setReadingType(Integer.parseInt(reading.getReadingType()));
		readingDO.setStatus("P");
		readingDO.setValue(Integer.parseInt(reading.getValue()));
				
		readingDO.setTimeTaken(makeTimestamp(reading.getDate(), reading.getTimeTaken()));
		
		int readingId = save(readingDO);
	}
	
	private static Timestamp makeTimestamp(String date, String time){
		
		String[] dateTokens = date.split("-");
		
		String[] timeTokens = time.split(":");
		
		Timestamp stamp = new Timestamp(
				Integer.parseInt(dateTokens[0]) - 1900,
				Integer.parseInt(dateTokens[1]),
				Integer.parseInt(dateTokens[2]),
				Integer.parseInt(timeTokens[0]),
				Integer.parseInt(timeTokens[1]),
				0, 0);
		
		return stamp;
	}
	
	public static List<GlucoReading> getPastReadingsByUser(int uid, int type, int num){
		
		List<?> readingList = getLatestByUser(uid, type, num);
		
		List<GlucoReading> result = new ArrayList<GlucoReading>();
		
		for (int i = 0; i < readingList.size(); i++){
			GlucoReadingDO reading = (GlucoReadingDO) readingList.get(i);
			
			result.add(transformFromGlucoReadingDO(reading));
		}
		
		return result;
	}
	
	private static GlucoReading transformFromGlucoReadingDO(GlucoReadingDO glucoReadingDO) {
		
		if (glucoReadingDO != null){
			GlucoReading glucoReading = new GlucoReading();
			glucoReading.setFirstName(glucoReadingDO.getUser().getFirstName());
			glucoReading.setLastName(glucoReadingDO.getUser().getLastName());
			glucoReading.setTimeTaken(glucoReadingDO.getTimeTaken().toString());
			glucoReading.setId(glucoReadingDO.getId().toString());
			glucoReading.setReadingType(glucoReadingDO.getReadingType().toString());
			glucoReading.setUid(glucoReadingDO.getUser().getId().toString());
			glucoReading.setValue(glucoReadingDO.getValue().toString());
			
			if (!glucoReadingDO.getStatus().equalsIgnoreCase("P")){
				List<?> commentList = getComment(glucoReadingDO.getId());
				
				for (int i = 0; i < commentList.size(); i++){
					GlucoReadingCommentsDO comment = (GlucoReadingCommentsDO) commentList.get(i);
					
					glucoReading.setComment(comment.getComment());
				}
			}
			
			return glucoReading;
		}
		
		return null;
	}
	
	private static List<?> getLatestByUser(int uid, int type, int num) {
	    SessionFactory sf = HibernateUtil.getSessionFactory();
	    Session session = sf.openSession();
	    	 	    	         	     
	    Query query = session.createQuery("from GlucoReadingDO where status = 'R' and user_id = :uid and type = :type order by time_taken");
	    query.setParameter("uid", uid);
	    query.setParameter("type", type);
	    query.setMaxResults(num); 
	    
	    List<?> list = query.list();
	    session.close();
	 
	    return list;
	}
	
	private static GlucoReadingScheduleDO getActiveVitalsSchedule(Integer uid) {
	    SessionFactory sf = HibernateUtil.getSessionFactory();
	    Session session = sf.openSession();
	    	 	    	         	     
	    Query query = session.createQuery("from GlucoReadingScheduleDO where status = 'A' and user_id = :userId");
	    query.setParameter("userId", uid);
	    
	    List<GlucoReadingScheduleDO> list = query.list();
	    
	    session.close();
	 
	    return list.isEmpty() ? null : list.get(0);
	}
	
	private static List<?> getAllVitalsSchedule(Integer uid) {
	    SessionFactory sf = HibernateUtil.getSessionFactory();
	    Session session = sf.openSession();
	    	 	    	         	     
	    Query query = session.createQuery("from GlucoReadingScheduleDO where user_id = :userId");
	    query.setParameter("userId", uid);
	    
	    List<GlucoReadingScheduleDO> list = query.list();
	    
	    session.close();
	 
	    return list;
	}
	
	private static GlucoReadingScheduleDO getPendingVitalsSchedules(Integer uid) {
	    SessionFactory sf = HibernateUtil.getSessionFactory();
	    Session session = sf.openSession();
	    	 	    	         	     
	    Query query = session.createQuery("from GlucoReadingScheduleDO where status = 'P' and user_id = :userId order by start_date ASC");
	    query.setParameter("userId", uid);
	    
	    List<GlucoReadingScheduleDO> list = query.list();
	    
	    session.close();
	 
	    return list.isEmpty() ? null : list.get(0);
	}
	
	private static List<?> getComment(Integer readingId) {
	    SessionFactory sf = HibernateUtil.getSessionFactory();
	    Session session = sf.openSession();
	    	 	    	         	     
	    Query query = session.createQuery("from GlucoReadingCommentsDO where glucose_reading_id = :readingId");
	    query.setParameter("readingId", readingId);
	    
	    List<?> list = query.list();
	    session.close();
	 
	    return list;
	}
	
	private static List<?> getLatest(int num, String status) {
	    SessionFactory sf = HibernateUtil.getSessionFactory();
	    Session session = sf.openSession();
	    	 	    	         	     
	    Query query = session.createQuery("from GlucoReadingDO where status = :status order by time_taken desc");
	    query.setParameter("status", status);
	    query.setMaxResults(num); 
	    
	    List<?> list = query.list();
	    session.close();
	 
	    return list;
	}
	
	private static int save(GlucoReadingScheduleDO schedule) {
	    SessionFactory sf = HibernateUtil.getSessionFactory();
	    Session session = sf.openSession();
	    
	    Query query = session.createQuery("from GlucoReadingScheduleDO where status = 'A' and user_id = :userId");
	    query.setParameter("userId", schedule.getUserId());
	    
	    List<GlucoReadingScheduleDO> list = query.list();
	    
	    session.beginTransaction();

	    if (!list.isEmpty()){
	    	// There is an active schedule right now
	    	GlucoReadingScheduleDO active = list.get(0);
	    	
	    	Calendar cal = Calendar.getInstance();
	    	cal.setTimeInMillis(schedule.getStartDate().getTime());
	    	cal.add(Calendar.DATE, -1);	    	
	    	
	    	active.setEndDate(new Date(cal.getTimeInMillis()));
	    		    	
	    	session.save(active);
	    	
	    	schedule.setStatus("P");
	    	session.save(schedule);
	    	
	    }
	    else {	    	
	    	session.save(schedule);	
	    }
	    
	    session.getTransaction().commit();
	    
	    session.close();
	    
	    return schedule.getId();
	}
	
	private static int update(GlucoReadingScheduleDO schedule, GlucoReadingScheduleDO newSchedule) {
	    SessionFactory sf = HibernateUtil.getSessionFactory();
	    Session session = sf.openSession();    
	    session.beginTransaction();
    	
	    session.save(schedule);	
	    session.save(newSchedule);	
	    
	    session.getTransaction().commit();
	    
	    session.close();
	    
	    return schedule.getId();
	}
	
	private static int save(GlucoReadingDO reading) {
	    SessionFactory sf = HibernateUtil.getSessionFactory();
	    Session session = sf.openSession();
	    session.beginTransaction();
	    
	    session.save(reading);
	         
	    session.getTransaction().commit();
	         
	    session.close();
	    
	    return reading.getId();
	}
	
	private static int save(GlucoReadingCommentsDO comment, String type) {
	    SessionFactory sf = HibernateUtil.getSessionFactory();
	    Session session = sf.openSession();
	    session.beginTransaction();
	    
	    Integer readingId = comment.getGlucoseReadingId();
	    
	    Query query = session.createQuery("from GlucoReadingDO where id = :id");
	    query.setParameter("id", readingId);
	    
	    List<?> list = query.list();
	    GlucoReadingDO reading = (GlucoReadingDO) list.get(0);
	    
	    if (type.equalsIgnoreCase("C")){
	    	reading.setStatus("R");
	    }
	    else if (type.equalsIgnoreCase("F")){
	    	reading.setStatus("F");
	    }
	    
	    session.save(reading);
	 
	    int id = (Integer) session.save(comment);
	    comment.setId(id);
	         
	    session.getTransaction().commit();
	         
	    session.close();
	    
	    return comment.getId();
	}
}
