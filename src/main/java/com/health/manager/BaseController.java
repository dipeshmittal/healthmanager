package com.health.manager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.QueryParam;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.health.manager.dal.GlucoReadingsDALManager;
import com.health.manager.dal.UserDALManager;
import com.health.manager.hibernate.HibernateUtil;
import com.health.manager.hibernate.UserDO;

@Controller
@RequestMapping("/")
public class BaseController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String welcome(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "index";

	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "login";

	}
	
	@RequestMapping(value = "/adminDashboard", method = RequestMethod.GET)
	public String adminDashboard(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "forms/adminDashboard";

	}
	
	@RequestMapping(value = "/educator", method = RequestMethod.GET)
	public String educator(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "educator";

	}
	
	@RequestMapping(value = "/customuser/create", method = RequestMethod.GET)
	public String openAddUserForm(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "addUser";

	}
	
	@RequestMapping(value = "/customuser/createkin", method = RequestMethod.GET)
	public String openAddUserkinForm(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "addUserKin";

	}
	
	@RequestMapping(value = "/customuser/createHealthInfo", method = RequestMethod.GET)
	public String openAddUserHealthInfoForm(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "addUserHealthInfo";

	}
	
	@RequestMapping(value = "/customuser/forms/createHealthInfo", method = RequestMethod.GET)
	public String openAddUserHealthInfoForm2(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "addUserHealthInfo";

	}
	
	@RequestMapping(value = "/customuser/upload", method = RequestMethod.GET)
	public String openUploadDataForm(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "uploadFiles";

	}
	
	@RequestMapping(value = "/customuser/find", method = RequestMethod.GET)
	public String openFindUserForm(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "findUser";

	}
	
	@RequestMapping(value = "/customuser/addGlucoData", method = RequestMethod.GET)
	public String openGlucoDataForm(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "addGlucoData";

	}
	
	@RequestMapping(value = "/customuser/addVitalsSchedule", method = RequestMethod.GET)
	public String openVitalsScheduleForm(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "addVitalsSchedule";

	}
	
	@RequestMapping(value = "/customuser/addVitalsSchedule", method = RequestMethod.POST)
    public String saveVitalsSchedule(
            @ModelAttribute("glucoReadingSchedule") GlucoReadingSchedule schedule,
                    Model map) {
         
		GlucoReadingsDALManager.addGlucoReadingSchedule(schedule);
		
        return "addVitalsSchedule";
    }
	
	@RequestMapping(value = "/customuser/uploadDocs", method = RequestMethod.POST)
    public String saveFile(
            @ModelAttribute("uploadForm") FileUploadForm uploadForm,
                    Model map) {
         
        List<MultipartFile> files = uploadForm.getFiles();
 
        List<String> fileNames = new ArrayList<String>();
         
        if(null != files && files.size() > 0) {
            for (MultipartFile multipartFile : files) {
 
                String fileName = multipartFile.getOriginalFilename();
                DropBoxUtil.upLoadFileToDropBox(uploadForm.getUid(), uploadForm.getPurpose(), 
                		uploadForm.getFileName().isEmpty() ? null : uploadForm.getFileName(), uploadForm.getDate(), multipartFile);
                
                fileNames.add(fileName);
                //Handle file content - multipartFile.getInputStream()
 
            }
        }
         
        map.addAttribute("files", fileNames);
        return "findUser";
    }
	
	@RequestMapping(value = "/customuser/addGlucoData", method = RequestMethod.POST)
    public String saveGlucoData(
            @ModelAttribute("glucoReading") GlucoReading reading,
                    Model map) {
         
		GlucoReadingsDALManager.addGlucoReading(reading);
		
        return "findUser";
    }
	
	@RequestMapping(value = "/customuser/viewUserInfo", method = RequestMethod.POST)
	public String openUserInfoView(@ModelAttribute("user") 
											User formUser, ModelMap model) {
		
		User user = null;
		if (formUser.getUid() != null && !formUser.getUid().isEmpty()){
			System.out.println("UID is : " + formUser.getUid());
			
			user = UserDALManager.findUser(formUser.getUid(), true);
		}
		else if (formUser.getEmail() != null && !formUser.getEmail().isEmpty()){
			System.out.println("Email is : " + formUser.getEmail());
			
			user = UserDALManager.findUserByEmail(formUser.getEmail());
		}
		else {
			// Write logic later for fetch by name
		}
		
		addGlucoData(model, user);
		model.addAttribute("name", user.getFirstName() + " " + user.getLastName());
		model.addAttribute("email", user.getEmail());
		model.addAttribute("gender", user.getGender());
		model.addAttribute("phone_number", user.getPhoneNumber());
		model.addAttribute("uid", user.getUid());
		model.addAttribute("signup_date", user.getSignUpDate());
		model.addAttribute("address", user.getAddressStreet() + ", " + 
				user.getAddressCity() + ", Pincode - " + user.getAddressPincode());
		model.addAttribute("type_diabetes", user.getHealthInfo().getTypeDiabetes());
		model.addAttribute("diabetes_since_date", user.getHealthInfo().getDateOfDiagnosis());
		model.addAttribute("date_of_birth", user.getDateOfBirth());
		model.addAttribute("gluco_device_name", user.getHealthInfo().getGlucoDevice());
		//getVitalsSchedule(user.getUid());

		/*
		 * next_gluco_test_due_date, last_patient_contact_date, next_followup_date
		 * diabetes_educator_name, dietician_name, next_lab_test_name, next_lab_test_date
		 * 
		 * 
		 * */
		return "viewUserInfo";

	}
	
	@RequestMapping(value = "/getFilesMetaData", method = RequestMethod.POST)
	public @ResponseBody String getFilesMetaData(@ModelAttribute("userFile") 
									UserFile userFile, ModelMap model) {

		System.out.println("####### : " + userFile.getUid());
		List<UserFile> files = DropBoxUtil.getMetadata(userFile.getUid());
		
		return addFileData(files, model);
		
	}
	
	@RequestMapping(value = "/customuser/getVitalsSchedule", method = RequestMethod.GET)
	public @ResponseBody String getVitalsSchedule(@RequestParam("uid") 
									String uid, ModelMap model) {

		System.out.println("####### : " + uid);
		
		User user = UserDALManager.findUser(uid.trim(), false);
		
		if (user == null){
			return makeErrorObject("User not found. Please enter a valid Kronica ID");
		}
		
		List<GlucoReadingSchedule> list = GlucoReadingsDALManager.getAllVitalsSchedule(uid);
		
		
		if (list == null || list.isEmpty()){
			return makeErrorObject("No schedules found for this user");
		}
		
		JSONObject schedules = new JSONObject();
		JSONArray pending = new JSONArray();
		
		for (int i = 0; i < list.size(); i++){
			if (list.get(i).getIsActive().equalsIgnoreCase("Y")){
				schedules.put("active", makeVitalsJSONObject(list.get(i)));
			}
			else {
				pending.add(makeVitalsJSONObject(list.get(i)));
			}
		}
		schedules.put("pending", pending);
		
		JSONObject result = new JSONObject();
		result.put("schedules", schedules);
		
		return result.toJSONString();
		
	}
	
	private JSONObject makeVitalsJSONObject(GlucoReadingSchedule schedule){
		
		JSONObject obj = new JSONObject();
		obj.put("start_date", schedule.getApplyDate());
		obj.put("end_date", schedule.getEndDate());
		obj.put("sunday", schedule.isSunday());
		obj.put("monday", schedule.isMonday());
		obj.put("tuesday", schedule.isTuesday());
		obj.put("wednesday", schedule.isWednesday());
		obj.put("thursday", schedule.isThursday());
		obj.put("friday", schedule.isFriday());
		obj.put("saturday", schedule.isSaturday());
		
		obj.put("fasting", schedule.isFasting());
		obj.put("postBreak", schedule.isPostBreak());
		obj.put("preLunch", schedule.isPreLunch());
		obj.put("postLunch", schedule.isPostLunch());
		obj.put("preDinner", schedule.isPreDinner());
		obj.put("postDinner", schedule.isPostDinner());
		
		return obj;
		
	}
	
	private String makeErrorObject(String err){
		
		JSONObject targetObject = new JSONObject();

		targetObject.put("error", err);
		
		return targetObject.toJSONString();
	}
	
	private void getVitalsSchedule(String uid){
		
		String pattern = GlucoReadingsDALManager.getVitalsSchedule(uid);
		
		Calendar cal = Calendar.getInstance();
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		System.out.println("Day of week : " + dayOfWeek);
		System.out.println("Weekly Pattern : " + pattern);
		String dayPattern = pattern.substring((dayOfWeek-1)*6, dayOfWeek*6);
		
		System.out.println("Today's pattern : " + dayPattern);
	}
	
	@RequestMapping(value = "/getFile", method = RequestMethod.POST)
	public @ResponseBody String getFile(@ModelAttribute("userFile") 
									UserFile userFile, ModelMap model) {

		System.out.println("####### : " + userFile.getPath());
		DropBoxUtil.getFile(userFile.getPath(), userFile.getType());

		JSONObject targetObject = new JSONObject();

		targetObject.put("path", "/files/" + userFile.getType() + ".pdf");
	
		String res = targetObject.toJSONString();
		
		System.out.println(res);
		return res;			
	}
	
	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String about(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "about";

	}
	
	@RequestMapping(value = "/forms", method = RequestMethod.GET)
	public String adminForms(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "forms";

	}
	
	@RequestMapping(value = "/education", method = RequestMethod.GET)
	public String educate(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "education";

	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(ModelMap model) {

		model.addAttribute("message",
				"Maven Web Project");

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "signup";

	}
	
	@RequestMapping(value = "customuser/forms/newUser", method = RequestMethod.POST)
	public String newUser(@ModelAttribute("user") 
									User user, ModelMap model) {

		System.out.println(user.getEmail() + " " + 
							user.getPassword() + " " + 
							user.getConfirmPassword() + " " + 
							user.getFirstName() + " " + 
							user.getLastName() + " " + 
							user.getDateOfBirth());
		
		
		int userId = UserDALManager.createUser(user);
		model.put("primaryUserId", userId);

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "addUserKin";
				
	}
	
	@RequestMapping(value = "customuser/forms/newSecondaryUser", method = RequestMethod.POST)
	public String newSecondaryUser(@ModelAttribute("secondaryUser") 
									SecondaryUser user, ModelMap model) {

		System.out.println(user.getEmail() + " " + 
							user.getFirstName() + " " + 
							user.getLastName() + " " + 
							user.getDateOfBirth() + " " +
							user.getPhoneNumber() + " " + 
							user.getPrimaryUserId() + " " +
							user.getRelationshipWithPrimaryUser());
		
		
		int userId = UserDALManager.createSecondaryUser(user);
		
		model.put("primaryUserId", user.getPrimaryUserId());

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "addUserHealthInfo";
				
	}
	
	@RequestMapping(value = "customuser/forms/createHealthInfo", method = RequestMethod.POST)
	public String newUserHealthInfo(@ModelAttribute("userHealthInfo") 
									UserHealthInfo userInfo, ModelMap model) {
		
		UserDALManager.createUserHealthInfo(userInfo);
		model.put("primaryUserId", "K-000" + userInfo.getPrimaryUserId());

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "signupComplete";
				
	}
	
	@RequestMapping(value = "addCommentForReading", method = RequestMethod.POST)
	public String newGlucoReadingComment(@ModelAttribute("readingComment") 
									GlucoReading readingComment, ModelMap model) {

		System.out.println("Something to print: " + readingComment.getComment() + readingComment.getId());

		GlucoReadingsDALManager.addCommentOnReading(readingComment, "C");
		
		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "educator";
				
	}
	
	@RequestMapping(value = "addFlagForReading", method = RequestMethod.POST)
	public String newGlucoReadingFlag(@ModelAttribute("readingComment") 
									GlucoReading readingComment, ModelMap model) {

		System.out.println("Something to print: " + readingComment.getComment() + readingComment.getId());

		GlucoReadingsDALManager.addCommentOnReading(readingComment, "F");
		
		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "educator";
				
	}
	
	@RequestMapping(value = "/customuser/findUser", method = RequestMethod.GET)
	public @ResponseBody String findUser(@RequestParam("uid") 
									String uid, ModelMap model) {

		System.out.println(uid);
		
		User user = UserDALManager.findUser(uid.trim(), false);
						
		JSONObject obj = new JSONObject();
		
		if (user != null){
			obj.put("patient_name", user.getFirstName() + " " + user.getLastName());
		}
		else {
			obj.put("patient_name", "User not found. Please enter a valid Kronica-ID.");
		}
		
		System.out.println(obj.toJSONString());
		
		return obj.toJSONString();
	}
	
	private String addFileData(List<UserFile> files, ModelMap model){
		
		if (files != null){
			
			JSONObject targetObject = new JSONObject();
			
			JSONArray reportArray = new JSONArray();
			JSONArray presArray = new JSONArray();
			JSONArray dietArray = new JSONArray();
			JSONArray exerciseArray = new JSONArray();
			
			if (!files.isEmpty()){
				for (UserFile file: files){
					JSONObject obj = new JSONObject();
					obj.put("uid", file.getUid());
					obj.put("path", file.getPath());
					obj.put("name", file.getName());
					obj.put("date", file.getDate());
					obj.put("type", file.getType());
					
					if (file.getType().equalsIgnoreCase("prescriptions")){
						presArray.add(obj);
					}
					else if (file.getType().equalsIgnoreCase("labReports")){
						reportArray.add(obj);
					}
					else if (file.getType().equalsIgnoreCase("dietPlans")){
						dietArray.add(obj);
					}
					else if (file.getType().equalsIgnoreCase("exercisePlans")){
						exerciseArray.add(obj);
					}
					else {
						reportArray.add(obj);
					}
					
				}
			}
			targetObject.put("reports", reportArray);
			targetObject.put("prescriptions", presArray);
			targetObject.put("exercise_plans", exerciseArray);
			targetObject.put("diet_plans", dietArray);
					
			String res = targetObject.toJSONString();
			
			System.out.println(res);
			return res;
		}
		
		return null;
	}
	
	private void addGlucoData(ModelMap model, User user){
		
		if (user != null){
			if (user.getFasting() != null && !user.getFasting().isEmpty()){
				for (int i = 0; i < user.getFasting().size(); i++){
					model.addAttribute("fasting_date_" + i, user.getFasting().get(i).getTimeTaken());
					model.addAttribute("fasting_reading_" + i, user.getFasting().get(i).getValue());
				}
			}
			
			if (user.getPreLunch() != null && !user.getPreLunch().isEmpty()){
				for (int i = 0; i < user.getPreLunch().size(); i++){
					model.addAttribute("pre_lunch_date_" + i, user.getPreLunch().get(i).getTimeTaken());
					model.addAttribute("pre_lunch_reading_" + i, user.getPreLunch().get(i).getValue());
				}
			}
			
			if (user.getPreDinner() != null && !user.getPreDinner().isEmpty()){
				for (int i = 0; i < user.getPreDinner().size(); i++){
					model.addAttribute("pre_dinner_date_" + i, user.getPreDinner().get(i).getTimeTaken());
					model.addAttribute("pre_dinner_reading_" + i, user.getPreDinner().get(i).getValue());
				}
			}
			
			if (user.getPostBreakFast() != null && !user.getPostBreakFast().isEmpty()){
				for (int i = 0; i < user.getPostBreakFast().size(); i++){
					model.addAttribute("post_breakfast_date_" + i, user.getPostBreakFast().get(i).getTimeTaken());
					model.addAttribute("post_breakfast_reading_" + i, user.getPostBreakFast().get(i).getValue());
				}
			}
			
			if (user.getPostLunch() != null && !user.getPostLunch().isEmpty()){
				for (int i = 0; i < user.getPostLunch().size(); i++){
					model.addAttribute("post_lunch_date_" + i, user.getPostLunch().get(i).getTimeTaken());
					model.addAttribute("post_lunch_reading_" + i, user.getPostLunch().get(i).getValue());
				}
			}
			
			if (user.getPostDinner() != null && !user.getPostDinner().isEmpty()){
				for (int i = 0; i < user.getPostDinner().size(); i++){
					model.addAttribute("post_dinner_date_" + i, user.getPostDinner().get(i).getTimeTaken());
					model.addAttribute("post_dinner_reading_" + i, user.getPostDinner().get(i).getValue());
				}
			}
			
			if (user.getRandom() != null && !user.getRandom().isEmpty()){
				for (int i = 0; i < user.getRandom().size(); i++){
					model.addAttribute("random_date_" + i, user.getRandom().get(i).getTimeTaken());
					model.addAttribute("random_reading_" + i, user.getRandom().get(i).getValue());
				}
			}
			
			if (user.getPostMeds() != null && !user.getPostMeds().isEmpty()){
				for (int i = 0; i < user.getPostMeds().size(); i++){
					model.addAttribute("post_meds_date_" + i, user.getPostMeds().get(i).getTimeTaken());
					model.addAttribute("post_meds_reading_" + i, user.getPostMeds().get(i).getValue());
				}
			}
		}
	}
	
	@RequestMapping(value = "/signin", method = RequestMethod.POST)
	public String signin(@ModelAttribute("user") User user, ModelMap model) {

		model.addAttribute("message",
				"Maven Web Project");
		
		System.out.println(user.getEmail() + " : "+ user.getPassword());
		
		User resultUser = UserDALManager.login(user.getEmail(), user.getPassword());
		
		if (resultUser != null && resultUser.getType().equalsIgnoreCase("6")){
			return "educator";
		}
		
		return "login";
	}
	
	@RequestMapping(value = "/addPrescription", method = RequestMethod.POST)
	public String createPrescription(@ModelAttribute("prescription") 
											Prescription prescription, ModelMap model) {

		System.out.println("SessionToken : " + prescription.getSessionToken());
		System.out.println("Date : " + prescription.getDate());		
		System.out.println("Symptoms : " + prescription.getSymptoms());
		System.out.println("Doctor's Name : " + prescription.getDoctor());
		
		UserManager manager = new UserManager();
		String userId = null;
		String userSession = null;
		
		try {
			userSession = manager.getUserFromSessionToken(prescription.getSessionToken());
						
			JSONObject jObject = (JSONObject) JSONValue.parse(userSession);
			
			userId = (String) jObject.get("objectId");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (userId == null){
			System.out.println("Invalid Session Token");
			model.addAttribute("errorMessage", "Please login again");
			return "login";
		}
		else {
			System.out.println("Create prescription for : " + userId);
			prescription.setUserId(userId);
			
			try {
				manager.addPrescription(prescription);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			JSONObject jObject = (JSONObject) JSONValue.parse(userSession);
			
			System.out.println("Inside Dashboard setup call");
			model.addAttribute("userId", (String) jObject.get("objectId"));
			model.addAttribute("token", (String) jObject.get("sessionToken"));
			model.addAttribute("firstName", (String) jObject.get("firstName"));
			model.addAttribute("lastName", (String) jObject.get("lastName"));
			
			return "dashboard";
		}
		
	}
	
	@RequestMapping(value = "/fetchHistory", method = RequestMethod.GET)
	public @ResponseBody String loadPastReadingsForUser(ModelMap model, @RequestParam("uid") String uid,
												@RequestParam("type") String type) {
		
		List<GlucoReading> readingList = GlucoReadingsDALManager.getPastReadingsByUser(Integer.parseInt(uid), 
				Integer.parseInt(type), 5);
		
		return formatReadingsData(readingList);
				
	}
	
	@RequestMapping(value = "/fetchGlucoseData", method = RequestMethod.GET)
	public @ResponseBody String loadGlucoseData(ModelMap model) {
		
		List<GlucoReading> newReadingList = GlucoReadingsDALManager.getLatestReadings(5);
		List<GlucoReading> flaggedReadingList = GlucoReadingsDALManager.getLatestFlaggedReadings(5);
		List<GlucoReading> reviewedReadingList = GlucoReadingsDALManager.getLatestReviewedReadings(5);
		
		return formatReadingsDataforEducator(newReadingList, flaggedReadingList, reviewedReadingList);
				
	}
	
	@RequestMapping(value = "/forms/loadUsers", method = RequestMethod.GET)
	public @ResponseBody String loadUsers(ModelMap model) {
				
		List<User> userList = UserDALManager.listAllPatients();
		
		return formatUserData(userList);
	}
	
	private static String formatUserData(List<User> users){
		
		if (users != null){
			
			JSONObject targetObject = new JSONObject();
			
			JSONArray userArray = new JSONArray();
			
			if (!users.isEmpty()){
				for (User user: users){
					JSONObject obj = new JSONObject();
					obj.put("email", user.getEmail());
					obj.put("first_name", user.getFirstName());
					obj.put("last_name", user.getLastName());
					obj.put("uid", user.getUid());
					
					userArray.add(obj);
				}
			}
			targetObject.put("users", userArray);
		
			String res = targetObject.toJSONString();
			
			System.out.println(res);
			return res;
		}
		
		return null;
	}
	
	private static String formatReadingsData(List<GlucoReading> readings){
		
		if (readings != null){
			
			JSONObject targetObject = new JSONObject();
			
			JSONArray newReadingArray = new JSONArray();
			
			if (!readings.isEmpty()){
				for (GlucoReading reading: readings){
					JSONObject obj = new JSONObject();
					obj.put("first_name", reading.getFirstName());
					obj.put("last_name", reading.getLastName());
					obj.put("uid", reading.getUid());
					obj.put("time_taken", reading.getTimeTaken());
					obj.put("value", reading.getValue());
					obj.put("reading_type", reading.getReadingType());
					obj.put("id", reading.getId());
					
					newReadingArray.add(obj);
				}
			}
			targetObject.put("new_readings", newReadingArray);
					
			String res = targetObject.toJSONString();
			
			System.out.println(res);
			return res;
		}
		
		return null;
	}
	
	private static String formatReadingsDataforEducator(List<GlucoReading> newReadings,
			List<GlucoReading> flaggedReadings, List<GlucoReading> reviewedReadings){
		
		JSONObject targetObject = new JSONObject();
		
		if (newReadings != null){
			
			JSONArray newReadingArray = new JSONArray();
			
			if (!newReadings.isEmpty()){
				for (GlucoReading reading: newReadings){
					JSONObject obj = new JSONObject();
					obj.put("first_name", reading.getFirstName());
					obj.put("last_name", reading.getLastName());
					obj.put("uid", reading.getUid());
					obj.put("time_taken", reading.getTimeTaken());
					obj.put("value", reading.getValue());
					obj.put("reading_type", reading.getReadingType());
					obj.put("id", reading.getId());
					
					if (reading.getComment() != null){
						obj.put("comment", reading.getComment());
					}
					
					newReadingArray.add(obj);
				}
			}
			targetObject.put("new_readings", newReadingArray);
		}
		
		if (flaggedReadings != null){
			
			JSONArray flaggedReadingArray = new JSONArray();
			
			if (!flaggedReadings.isEmpty()){
				for (GlucoReading reading: flaggedReadings){
					JSONObject obj = new JSONObject();
					obj.put("first_name", reading.getFirstName());
					obj.put("last_name", reading.getLastName());
					obj.put("uid", reading.getUid());
					obj.put("time_taken", reading.getTimeTaken());
					obj.put("value", reading.getValue());
					obj.put("reading_type", reading.getReadingType());
					obj.put("id", reading.getId());
					
					if (reading.getComment() != null){
						obj.put("comment", reading.getComment());
					}
					
					flaggedReadingArray.add(obj);
				}
			}
			targetObject.put("flagged_readings", flaggedReadingArray);
		}

		if (reviewedReadings != null){
			
			JSONArray reviewedReadingArray = new JSONArray();
			
			if (!reviewedReadings.isEmpty()){
				for (GlucoReading reading: reviewedReadings){
					JSONObject obj = new JSONObject();
					obj.put("first_name", reading.getFirstName());
					obj.put("last_name", reading.getLastName());
					obj.put("uid", reading.getUid());
					obj.put("time_taken", reading.getTimeTaken());
					obj.put("value", reading.getValue());
					obj.put("reading_type", reading.getReadingType());
					obj.put("id", reading.getId());
					
					if (reading.getComment() != null){
						obj.put("comment", reading.getComment());
					}
					
					reviewedReadingArray.add(obj);
				}
			}
			targetObject.put("reviewed_readings", reviewedReadingArray);
		}
		
		try {
			String res = targetObject.toJSONString();
			
			System.out.println(res);
			return res;
		}
		catch (Exception e){
			return null;
		}
		
	}

	private String loadPrescriptions(UserManager manager, String userId) {
		System.out.println("Reading prescriptions for : " + userId);
		String prescriptions = null;
		try {
			prescriptions = manager.fetchPrescriptions(userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prescriptions;
	}
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String loginAndOpenDashBoard(@RequestParam("token") String token, @RequestParam("firstName") String firstName,
										@RequestParam("lastName") String lastName, @RequestParam("userId") String userId,
										ModelMap model) {

		System.out.println("Inside Dashboard setup call");
		model.addAttribute("userId", userId);
		model.addAttribute("token", token);
		model.addAttribute("firstName", firstName);
		model.addAttribute("lastName", lastName);

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "dashboard";

	}
	
	@RequestMapping(value = "/education/articles/", method = RequestMethod.GET)
	public String getArticles(ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "articles/articles";

	}	
	
	@RequestMapping(value = "/education/articles/{article_num}", method = RequestMethod.GET)
	public String openArticle(@PathVariable("article_num") String articleNum, ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "articles/article_" + articleNum;

	}
	
	@RequestMapping(value = "/forms/{form_num}", method = RequestMethod.GET)
	public String openForm(@PathVariable("form_num") String formNum, ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "forms/" + formNum;

	}
	
	@RequestMapping(value = "/data/{dataPage_num}", method = RequestMethod.GET)
	public String openDataPage(@PathVariable("dataPage_num") String dataPageNum, ModelMap model) {

		// Spring uses InternalResourceViewResolver and return back index.jsp
		return "data/" + dataPageNum;

	}

}