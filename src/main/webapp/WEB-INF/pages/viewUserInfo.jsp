<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">

    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="/css/plugins/dataTables.bootstrap.css" rel="stylesheet">

</head>
<body>
<div>
	<%@ include file="include/header.jsp" %> 
    <!--BEGIN BACK TO TOP-->
    <a id="totop" href="#"><i class="fa fa-angle-up"></i></a><!--END BACK TO TOP--><!--BEGIN TOPBAR-->
    <div id="header-topbar-option-demo" class="page-header-topbar">
        <%@ include file="include/navigation.html" %> 
        </div>
    <!--END TOPBAR-->
    <div id="wrapper">
    <!--BEGIN SIDEBAR MENU-->
        <nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
		   <div class="sidebar-collapse menu-scroll">
		       <ul id="side-menu" class="nav">
		           <li class="user-panel">
		               <div class="thumb"><img src="/images/ujjwal.jpg" alt="" class="img-circle"/></div>
		               <div class="info"><p>Ujjwal</p>
		                   <ul class="list-inline list-unstyled">
		                       <li><a href="#" data-hover="tooltip" title="Profile"><i class="fa fa-user"></i></a></li>
		                       <li><a href="#" data-hover="tooltip" title="Mail"><i class="fa fa-envelope"></i></a></li>
		                       <li><a href="#" data-hover="tooltip" title="Setting" data-toggle="modal" data-target="#modal-config"><i class="fa fa-cog"></i></a></li>
		                       <li><a href="#" data-hover="tooltip" title="Logout"><i class="fa fa-sign-out"></i></a></li>
		                   </ul>
		               </div>
		               <div class="clearfix"></div>
		           </li>
		           <li><a href="/educator"><i class="fa fa-tachometer fa-fw">
		               <div class="icon-bg bg-orange"></div>
		           </i><span class="menu-title">New Readings</span></a></li>
		           
		
		           <li><a href="#"><i class="fa fa-th-list fa-fw">
		               <div class="icon-bg bg-blue"></div>
		           </i><span class="menu-title">Create Data</span><span class="fa arrow"></span></a>
		               <ul class="nav nav-second-level">
		                   <li><a href="/customuser/create"><i class="fa fa-th-large"></i><span class="submenu-title">Create New User</span></a></li>
		               </ul>
		               <ul class="nav nav-second-level">
		                   <li><a href="/customuser/upload"><i class="fa fa-th-large"></i><span class="submenu-title">Upload files for User</span></a></li>
		               </ul>
		           </li>
		           
		           
		           <li class="active"><a  href="#"><i class="fa fa-file-o fa-fw">
		               <div class="icon-bg bg-primary"></div>
		           </i><span class="menu-title">View/Edit User Details</span><span class="fa arrow"></span></a>
		           	<ul class="nav nav-second-level">
		                   <li class="active"><a href="/customuser/find"><i class="fa fa-user"></i><span class="submenu-title">View details</span></a></li>
		                   <li><a href="/customuser/addGlucoData"><i class="fa fa-lock"></i><span class="submenu-title">Add Gluco Reading</span></a></li>
		               </ul>
		           </li>
		           
		           <li><a href="#"><i class="fa fa-file-o fa-fw">
		               <div class="icon-bg bg-primary"></div>
		           </i><span class="menu-title">Plan Patient activities</span><span class="fa arrow"></span></a>
		           	<ul class="nav nav-second-level">
		                   <li><a href="/customuser/addVitalsSchedule"><i class="fa fa-user"></i><span class="submenu-title">Schedule vitals readings</span></a></li>
		               </ul>
		           </li>
		
		           </ul>
		    </div>
		</nav>

        <!--END CHAT FORM--><!--BEGIN PAGE WRAPPER-->
        <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">${name}'s Information</div>
                </div>
                <div class="btn btn-blue reportrange"><i class="fa fa-calendar"></i>&nbsp;<span></span>&nbsp;report&nbsp;<i class="fa fa-angle-down"></i><input type="hidden" name="datestart"/><input type="hidden" name="endstart"/></div>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
            <div class="page-content">
                 <div class="row">
					<div class="col-lg-12">
						<div class="panel panel-green">
							<div class="panel-heading">Primary Information</div>
							<div class="row panel-body">
								<div class="col-lg-4">
									<p>Signup Date: ${signup_date}</p>
									<p>Next gluco test due on: ${next_gluco_test_due_date}</p>
									<p>Last patient contact: ${last_patient_contact_date}</p>
									<p>Next follow-up time: ${next_followup_date}</p>
								</div>
								<div class="col-lg-4">
									<p>Diabetes Educator: ${diabetes_educator_name}</p>
									<p>Dietician: ${dietician_name}</p>
									<p>Next lab test: ${next_lab_test_name}</p>
									<b>Next lab test on: ${next_lab_test_date}</b>
								</div>
								<div class="col-lg-4">
									<p>Kronica UID: ${uid}</p>
									<p>Gluco test device: ${gluco_device_name}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading">Personal Information</div>
							<div class="row panel-body">
								<div class="col-lg-4">
									<p>Email Address: ${email}</p>
									<p>Date of birth: ${date_of_birth}</p>
									<p>Gender: ${gender}</p>
								</div>
								<div class="col-lg-4">
									<p>Phone Number: ${phone_number}</p>
									<p>Address: ${address}</p>
								</div>
								<div class="col-lg-4">
									<p>Type of diabetes: ${type_diabetes}</p>
									<p>Diabetes since: ${diabetes_since_date}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- /.row -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								Diabetes Tracking Information
							</div>
							<div class="panel-body">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tabular1" data-toggle="tab">Pre-Meal</a>
									</li>
									<li><a href="#tabular2" data-toggle="tab">Post-Meal</a></li>
									<li><a href="#tabular3" data-toggle="tab">Others</a></li>
								</ul>
	
								<!-- Tab panes -->
								<div class="tab-content">
									<div class="tab-pane fade in active" id="tabular1">
									<br>
										<div class="row">
											<div class="col-lg-4">
												<div class="panel panel-default">
													<div class="panel-heading">Fasting Readings</div>
													<!-- /.panel-heading -->
													<div class="panel-body">
														<div class="table-responsive">
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th>Date</th>
																		<th>Value</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>${fasting_date_0}</td>
																		<td>${fasting_reading_0}</td>
																	</tr>
																	<tr>
																		<td>${fasting_date_1}</td>
																		<td>${fasting_reading_1}</td>
																	</tr>
																	<tr>
																		<td>${fasting_date_2}</td>
																		<td>${fasting_reading_2}</td>
																	</tr>
																	<tr>
																		<td>${fasting_date_3}</td>
																		<td>${fasting_reading_3}</td>
																	</tr>
																	<tr>
																		<td>${fasting_date_4}</td>
																		<td>${fasting_reading_4}</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.panel-body -->
												</div>
												<!-- /.panel -->
											</div>
											<div class="col-lg-4">
												<div class="panel panel-default">
													<div class="panel-heading">Pre-Lunch Readings</div>
													<!-- /.panel-heading -->
													<div class="panel-body">
														<div class="table-responsive">
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th>Date</th>
																		<th>Value</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>${pre_lunch_date_0}</td>
																		<td>${pre_lunch_reading_0}</td>
																	</tr>
																	<tr>
																		<td>${pre_lunch_date_1}</td>
																		<td>${pre_lunch_reading_1}</td>
																	</tr>
																	<tr>
																		<td>${pre_lunch_date_2}</td>
																		<td>${pre_lunch_reading_2}</td>
																	</tr>
																	<tr>
																		<td>${pre_lunch_date_3}</td>
																		<td>${pre_lunch_reading_3}</td>
																	</tr>
																	<tr>
																		<td>${pre_lunch_date_4}</td>
																		<td>${pre_lunch_reading_4}</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.panel-body -->
												</div>
												<!-- /.panel -->
											</div>
											<div class="col-lg-4">
												<div class="panel panel-default">
													<div class="panel-heading">Pre-Dinner Readings</div>
													<!-- /.panel-heading -->
													<div class="panel-body">
														<div class="table-responsive">
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th>Date</th>
																		<th>Value</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>${pre_dinner_date_0}</td>
																		<td>${pre_dinner_reading_0}</td>
																	</tr>
																	<tr>
																		<td>${pre_dinner_date_1}</td>
																		<td>${pre_dinner_reading_1}</td>
																	</tr>
																	<tr>
																		<td>${pre_dinner_date_2}</td>
																		<td>${pre_dinner_reading_2}</td>
																	</tr>
																	<tr>
																		<td>${pre_dinner_date_3}</td>
																		<td>${pre_dinner_reading_3}</td>
																	</tr>
																	<tr>
																		<td>${pre_dinner_date_4}</td>
																		<td>${pre_dinner_reading_4}</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.panel-body -->
												</div>
												<!-- /.panel -->
											</div>
										</div>
									</div>
									
									<div class="tab-pane fade in" id="tabular2">
									<br>
										<div class="row">
											<div class="col-lg-4">
												<div class="panel panel-default">
													<div class="panel-heading">Post-Breakfast Readings</div>
													<!-- /.panel-heading -->
													<div class="panel-body">
														<div class="table-responsive">
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th>Date</th>
																		<th>Value</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>${post_breakfast_date_0}</td>
																		<td>${post_breakfast_reading_0}</td>
																	</tr>
																	<tr>
																		<td>${post_breakfast_date_1}</td>
																		<td>${post_breakfast_reading_1}</td>
																	</tr>
																	<tr>
																		<td>${post_breakfast_date_2}</td>
																		<td>${post_breakfast_reading_2}</td>
																	</tr>
																	<tr>
																		<td>${post_breakfast_date_3}</td>
																		<td>${post_breakfast_reading_3}</td>
																	</tr>
																	<tr>
																		<td>${post_breakfast_date_4}</td>
																		<td>${post_breakfast_reading_4}</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.panel-body -->
												</div>
												<!-- /.panel -->
											</div>
											<div class="col-lg-4">
												<div class="panel panel-default">
													<div class="panel-heading">Post-Lunch Readings</div>
													<!-- /.panel-heading -->
													<div class="panel-body">
														<div class="table-responsive">
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th>Date</th>
																		<th>Value</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>${post_lunch_date_0}</td>
																		<td>${post_lunch_reading_0}</td>
																	</tr>
																	<tr>
																		<td>${post_lunch_date_1}</td>
																		<td>${post_lunch_reading_1}</td>
																	</tr>
																	<tr>
																		<td>${post_lunch_date_2}</td>
																		<td>${post_lunch_reading_2}</td>
																	</tr>
																	<tr>
																		<td>${post_lunch_date_3}</td>
																		<td>${post_lunch_reading_3}</td>
																	</tr>
																	<tr>
																		<td>${post_lunch_date_4}</td>
																		<td>${post_lunch_reading_4}</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.panel-body -->
												</div>
												<!-- /.panel -->
											</div>
											<div class="col-lg-4">
												<div class="panel panel-default">
													<div class="panel-heading">Post-Dinner Readings</div>
													<!-- /.panel-heading -->
													<div class="panel-body">
														<div class="table-responsive">
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th>Date</th>
																		<th>Value</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>${post_dinner_date_0}</td>
																		<td>${post_dinner_reading_0}</td>
																	</tr>
																	<tr>
																		<td>${post_dinner_date_1}</td>
																		<td>${post_dinner_reading_1}</td>
																	</tr>
																	<tr>
																		<td>${post_dinner_date_2}</td>
																		<td>${post_dinner_reading_2}</td>
																	</tr>
																	<tr>
																		<td>${post_dinner_date_3}</td>
																		<td>${post_dinner_reading_3}</td>
																	</tr>
																	<tr>
																		<td>${post_dinner_date_4}</td>
																		<td>${post_dinner_reading_4}</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.panel-body -->
												</div>
												<!-- /.panel -->
											</div>
										</div>
									</div>
									
									<div class="tab-pane fade in" id="tabular3">
									<br>
										<div class="row">
											<div class="col-lg-4">
												<div class="panel panel-default">
													<div class="panel-heading">Post-Medication/Insulin Readings</div>
													<!-- /.panel-heading -->
													<div class="panel-body">
														<div class="table-responsive">
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th>Date</th>
																		<th>Value</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>${post_meds_date_0}</td>
																		<td>${post_meds_reading_0}</td>
																	</tr>
																	<tr>
																		<td>${post_meds_date_1}</td>
																		<td>${post_meds_reading_1}</td>
																	</tr>
																	<tr>
																		<td>${post_meds_date_2}</td>
																		<td>${post_meds_reading_2}</td>
																	</tr>
																	<tr>
																		<td>${post_meds_date_3}</td>
																		<td>${post_meds_reading_3}</td>
																	</tr>
																	<tr>
																		<td>${post_meds_date_4}</td>
																		<td>${post_meds_reading_4}</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.panel-body -->
												</div>
												<!-- /.panel -->
											</div>
											<div class="col-lg-4">
												<div class="panel panel-default">
													<div class="panel-heading">Random Readings</div>
													<!-- /.panel-heading -->
													<div class="panel-body">
														<div class="table-responsive">
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th>Date</th>
																		<th>Value</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>${random_date_0}</td>
																		<td>${random_reading_0}</td>
																	</tr>
																	<tr>
																		<td>${random_date_1}</td>
																		<td>${random_reading_1}</td>
																	</tr>
																	<tr>
																		<td>${random_date_2}</td>
																		<td>${random_reading_2}</td>
																	</tr>
																	<tr>
																		<td>${random_date_3}</td>
																		<td>${random_reading_3}</td>
																	</tr>
																	<tr>
																		<td>${random_date_4}</td>
																		<td>${random_reading_4}</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.panel-body -->
												</div>
												<!-- /.panel -->
											</div>
										</div>
									</div>
									
									<div class="tab-pane fade" id="charts">
										<h4>Profile Tab</h4>
										<div class="demo-container">
											<div id="placeholder" class="demo-placeholder"></div>
										</div>
									</div>
								</div>
							</div>
	
						<!-- /.col-lg-6 -->
					</div>
				</div>
			</div>
			
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-danger">
						<div class="panel-heading">Lab Test Results</div>
						<div id="testReports" class="panel-body">
						</div>
					</div>
				</div>
			</div>
			
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Diet & Lifestyle Plan</div>
						<div class="panel-body">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#dietPlans" data-toggle="tab">Diet Plans</a>
									</li>
									<li><a href="#exercisePlans" data-toggle="tab">Exercise Plans</a></li>
								</ul>
	
								<!-- Tab panes -->
								<div class="tab-content">
									<div class="tab-pane fade in active" id="dietPlans">
									</div>
									<div class="tab-pane fade in" id="exercisePlans">
									</div>
								</div>
						</div>				
					</div>
				</div>
			</div>
			
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-success">
						<div class="panel-heading">Recommended Lab Tests Plan</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Lab test Plan</div>
										<!-- /.panel-heading -->
										<div class="panel-body">
											<div class="table-responsive">
												<table class="table table-striped table-bordered">
													<thead>
														<tr>
															<th>Test</th>
															<th>Body Part</th>
															<th>Frequency</th>
															<th>Last done</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>Lipid Profile</td>
															<td>Liver</td>
															<td>Once a month</td>
															<td>2014-08-19</td>
														</tr>
														<tr>
															<td>LSGT Profile</td>
															<td>Kidney</td>
															<td>Once every 3 months</td>
															<td>2014-07-23</td>
														</tr>
														<tr>
															<td>Echo</td>
															<td>Heart</td>
															<td>Once every 6 months</td>
															<td>2014-08-11</td>
														</tr>
														<tr>
															<td>Urine Profile</td>
															<td>Kidney</td>
															<td>Once every 2 weeks</td>
															<td>2014-08-29</td>
														</tr>
														<tr>
															<td>Thyroid Profile</td>
															<td>Thyroid</td>
															<td>Once a year</td>
															<td>2013-12-19</td>
														</tr>
													</tbody>
												</table>
											</div>
											<!-- /.table-responsive -->
										</div>
										<!-- /.panel-body -->
									</div>
									<!-- /.panel -->
								</div>
							</div>
	
						</div>
					</div>
				</div>
			</div>
                
            </div>
           </div>
        <!--BEGIN FOOTER-->
        <div id="footer">
            <div class="copyright">2014 © &mu;Admin - Responsive Multi-Style Admin Template</div>
        </div>
        <!--END FOOTER--><!--END PAGE WRAPPER--></div>
        <%@ include file="include/footer.jsp" %> 
	
    <script src="/js/jquery-1.11.0.js"></script>
    <script src="/js/underscore-min.js"></script>
    <script type="text/javascript" src="/js/json2.js"></script>

    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.0.0/backbone-min.js"></script>
    <script type="text/javascript" src="https://rawgithub.com/powmedia/backbone.bootstrap-modal/master/src/backbone.bootstrap-modal.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="/js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {

    	$.post( "/getFilesMetaData", { uid: '${uid}' }, function( data ) {
	    	console.log(JSON.stringify(data));

	    	var reports = data.reports; 
	    	if (reports){

	    		if (reports.length > 0){
	                var str = '';
	                str += "<tbody>";
	                for (var i = 0; i < reports.length; i++){
	                    str += "<tr>"+
                               "<td>" + reports[i].date + "</td>"+
                               "<td><a href='#' data-hover='tooltip' onclick='openFileWindow(" + JSON.stringify(reports[i]) + ")'>" + reports[i].name + "</a></td>" +
                               "</tr>";
	                }
	                str += "</tbody>";

	                var tableHead = "<thead><tr><th>Date</th><th>Report</th></tr></thead>";

	                $('#testReports').append("<table class='table table-striped table-bordered table-hover'>" + tableHead + str + "</table>");
	            }
	            else {
	            	$('#testReports').append("No reports found for user.");
	            }				
				
		    }

	    	var diet_plans = data.diet_plans; 
	    	if (diet_plans){

	    		if (diet_plans.length > 0){
	                var str = '';
	                str += "<tbody>";
	                for (var i = 0; i < diet_plans.length; i++){
	                    str += "<tr>"+
                               "<td>" + diet_plans[i].date + "</td>"+
                               "<td><a href='#' data-hover='tooltip' onclick='openFileWindow(" + JSON.stringify(diet_plans[i]) + ")'>" + diet_plans[i].name + "</a></td>" +
                               "</tr>";
	                }
	                str += "</tbody>";

	                var tableHead = "<thead><tr><th>Date</th><th>Diet Plan</th></tr></thead>";

	                $('#dietPlans').append("<table class='table table-striped table-bordered table-hover'>" + tableHead + str + "</table>");
	            }
	            else {
	            	$('#dietPlans').append("No diet plans found for user.");
	            }				
				
		    }

			var exercise_plans = data.exercise_plans;
	    	if (exercise_plans){

	    		if (exercise_plans.length > 0){
	                var str = '';
	                str += "<tbody>";
	                for (var i = 0; i < exercise_plans.length; i++){
	                    str += "<tr>"+
                               "<td>" + exercise_plans[i].date + "</td>"+
                               "<td><a href='#' data-hover='tooltip' onclick='openFileWindow(" + JSON.stringify(exercise_plans[i]) + ")'>" + exercise_plans[i].name + "</a></td>" +
                               "</tr>";
	                }
	                str += "</tbody>";

	                var tableHead = "<thead><tr><th>Date</th><th>Exercise Plan</th></tr></thead>";

	                $('#exercisePlans').append("<table class='table table-striped table-bordered table-hover'>" + tableHead + str + "</table>");
	            }
	            else {
	            	$('#exercisePlans').append("No exercise plans found for user.");
	            }				
				
		    }
	    }, "json");
	    
    });

    function openFileWindow(userfile){
        var file = eval(userfile);
		console.log("Path : " + file.path);
	    $.post( "/getFile", { path: file.path, type: file.type }, function( data ) {
	    	window.open(data.path, "Report", "width=1200, height=1000");
	    }, "json");
    }

    </script>

</div>
</body>
</html>