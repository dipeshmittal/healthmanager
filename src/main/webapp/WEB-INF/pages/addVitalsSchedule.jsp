<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">

    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="/css/plugins/dataTables.bootstrap.css" rel="stylesheet">

</head>
<body>
<div>
	<%@ include file="include/header.jsp" %> 
    <!--BEGIN BACK TO TOP-->
    <a id="totop" href="#"><i class="fa fa-angle-up"></i></a><!--END BACK TO TOP--><!--BEGIN TOPBAR-->
    <div id="header-topbar-option-demo" class="page-header-topbar">
        <%@ include file="include/navigation.html" %> 
        
     </div>
    <!--END TOPBAR-->
    <div id="wrapper">
    <!--BEGIN SIDEBAR MENU-->
        <nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
		   <div class="sidebar-collapse menu-scroll">
		       <ul id="side-menu" class="nav">
		           <li class="user-panel">
		               <div class="thumb"><img src="/images/ujjwal.jpg" alt="" class="img-circle"/></div>
		               <div class="info"><p>Ujjwal</p>
		                   <ul class="list-inline list-unstyled">
		                       <li><a href="#" data-hover="tooltip" title="Profile"><i class="fa fa-user"></i></a></li>
		                       <li><a href="#" data-hover="tooltip" title="Mail"><i class="fa fa-envelope"></i></a></li>
		                       <li><a href="#" data-hover="tooltip" title="Setting" data-toggle="modal" data-target="#modal-config"><i class="fa fa-cog"></i></a></li>
		                       <li><a href="#" data-hover="tooltip" title="Logout"><i class="fa fa-sign-out"></i></a></li>
		                   </ul>
		               </div>
		               <div class="clearfix"></div>
		           </li>
		           <li><a href="/educator"><i class="fa fa-tachometer fa-fw">
		               <div class="icon-bg bg-orange"></div>
		           </i><span class="menu-title">New Readings</span></a></li>
		           
		
		           <li><a href="#"><i class="fa fa-th-list fa-fw">
		               <div class="icon-bg bg-blue"></div>
		           </i><span class="menu-title">Create Data</span><span class="fa arrow"></span></a>
		               <ul class="nav nav-second-level">
		                   <li><a href="/customuser/create"><i class="fa fa-th-large"></i><span class="submenu-title">Create New User</span></a></li>
		               </ul>
		               <ul class="nav nav-second-level">
		                   <li><a href="/customuser/upload"><i class="fa fa-th-large"></i><span class="submenu-title">Upload files for User</span></a></li>
		               </ul>
		           </li>
		           
		           
		           <li><a  href="#"><i class="fa fa-file-o fa-fw">
		               <div class="icon-bg bg-primary"></div>
		           </i><span class="menu-title">View/Edit User Details</span><span class="fa arrow"></span></a>
		           	<ul class="nav nav-second-level">
		                   <li><a href="/customuser/find"><i class="fa fa-user"></i><span class="submenu-title">View details</span></a></li>
		                   <li><a href="/customuser/addGlucoData"><i class="fa fa-lock"></i><span class="submenu-title">Add Gluco Reading</span></a></li>
		               </ul>
		           </li>
		           
		           <li class="active"><a href="#"><i class="fa fa-file-o fa-fw">
		               <div class="icon-bg bg-primary"></div>
		           </i><span class="menu-title">Plan Patient activities</span><span class="fa arrow"></span></a>
		           	<ul class="nav nav-second-level">
		                   <li class="active"><a href="/customuser/addVitalsSchedule"><i class="fa fa-user"></i><span class="submenu-title">Schedule vitals readings</span></a></li>
		               </ul>
		           </li>
		
		           </ul>
		    </div>
		</nav>

        <!--END CHAT FORM--><!--BEGIN PAGE WRAPPER-->
        <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">Add Vitals Schedule</div>
                </div>
                <div class="btn btn-blue reportrange"><i class="fa fa-calendar"></i>&nbsp;<span></span>&nbsp;report&nbsp;<i class="fa fa-angle-down"></i><input type="hidden" name="datestart"/><input type="hidden" name="endstart"/></div>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
            <div class="page-content">
            	 <div class="row">
					<div class="col-lg-6">
                         <form id="getVitalsForm" action="#">
                             <div class="form-group">
                             	 <label>Kronica ID</label>
                             	 <br />
						         <input type="text" id="uid" />
                              </div>
						     <button id="getVitalsData" class="btn btn-default">Fetch Past Schedules</button>
						</form>
                     </div>
                 </div>
                 <div class="row">
					<div class="col-lg-6">
						<div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTable" id="currentVitalsData">
                            </table>
                        </div>
					</div>
				 </div>
				 <div class="row">
					<div class="col-lg-6">
						<button id="showVitalsForm" hidden="true">Add a schedule</button>
					</div>
				 </div>
                 <div id="vitalsForm" class="row" hidden='true'>
					<div class="col-lg-6">
                         <form method="post" action="addVitalsSchedule"
						        id="glucoReadingSchedule" enctype="multipart/form-data">
						 
						 	 <div class="form-group">
                                 <label>Kronica ID</label>
                                 <input class="form-control" id="uid" name="uid" type="text" autofocus>
                             </div>
                             <div class="form-group">
                             	 <label>Readings in a day</label>
                             	 <br />
						         <input type="checkbox" name="fasting" id="fasting" />
						         	<label for="fasting">Fasting</label>
						         	<br />
						         <input type="checkbox" name="postBreak" id="postBreak" />
						         	<label for="event">Post-Breakfast</label>
						        	<br />
						         <input type="checkbox" name="preLunch" id="preLunch" />
						         	<label for="message">Pre-Lunch</label>
						         	<br />
						         <input type="checkbox" name="postLunch" id="postLunch" />
						         	<label for="message">Post-Lunch</label>
						         	<br />
						         <input type="checkbox" name="preDinner" id="preDinner" />
						         	<label for="message">Pre-Dinner</label>
						         	<br />
						         <input type="checkbox" name="postDinner" id="postDinner" />
						         	<label for="message">Post-Dinner</label>
						         	<br />
                              </div>
                              <div class="form-group">
                             	 <label>Days to measure vitals in a week</label>
                             	 <br />
						         <input type="checkbox" name="sunday" id="sunday" />
						         	<label for="event">Sunday</label>
						        	<br />
						         <input type="checkbox" name="monday" id="monday" />
						         	<label for="message">Monday</label>
						         	<br />
						         <input type="checkbox" name="tuesday" id="tuesday" />
						         	<label for="message">Tuesday</label>
						         	<br />
						         <input type="checkbox" name="wednesday" id="wednesday" />
						         	<label for="message">Wednesday</label>
						         	<br />
						         <input type="checkbox" name="thursday" id="thursday" />
						         	<label for="message">Thursday</label>
						         	<br />
						         <input type="checkbox" name="fridau" id="friday" />
						         	<label for="fasting">Friday</label>
						         	<br />
						         <input type="checkbox" name="saturday" id="saturday" />
						         	<label for="fasting">Saturday</label>
						         	<br />
                              </div>
                             <div class="form-group">
								 <label>Apply from</label>
								 <input type="date" class="form-control" name="applyDate">
							 </div>
						     <button type="submit" class="btn btn-default">Add</button>
						</form>
                     </div>
                 </div>
                
            </div>
            </div>
        <!--BEGIN FOOTER-->
        <div id="footer">
            <div class="copyright">2014 © &mu;Admin - Responsive Multi-Style Admin Template</div>
        </div>
        <!--END FOOTER--><!--END PAGE WRAPPER--></div>
        <%@ include file="include/footer.jsp" %> 
	
    <script src="/js/jquery-1.11.0.js"></script>
    <script src="/js/underscore-min.js"></script>
    <script type="text/javascript" src="/js/json2.js"></script>

    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.0.0/backbone-min.js"></script>
    <script type="text/javascript" src="https://rawgithub.com/powmedia/backbone.bootstrap-modal/master/src/backbone.bootstrap-modal.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="/js/plugins/dataTables/dataTables.bootstrap.js"></script>

	<script>
	$('#getUser').on("click",function(){

		 $.get('./findUser?uid=' + $('#uid').val(), function(data,status){

	        alert(JSON.parse(data).patient_name);
        });
	});

	$('#getVitalsData').on("click",function(){

		 $.get('./getVitalsSchedule?uid=' + $('#uid').val(), function(data,status){

			var result = JSON.parse(data);
			if (result.error){
				alert(result.error);
			}
			else {
				alert(JSON.stringify(result.schedules));
				var str = '<thead><td>Start Date</td><td>Pattern</td><td>End Date</td></thead>';
				str += '<tbody>';

				if (result.schedules.active){
					str += '<tr><td>' + result.schedules.active.start_date + '</td><td>' + getPattern(result.schedules.active) + '</td><td>' + (result.schedules.active.end_date ? result.schedules.active.end_date : '') + '</td></tr>';
				}

		        if (result.schedules.pending){
		        	for (var i = 0; i < result.schedules.pending.length; i++){
			        	str += '<tr><td>' + result.schedules.pending[i].start_date + '</td><td>' + getPattern(result.schedules.pending[i]) + '</td><td>' + (result.schedules.pending[i].end_date ? result.schedules.pending[i].end_date : '') + '</td></tr>';
				    }
				}
		        
		        str += '</tbody>';
				$('#currentVitalsData').append(str);	
				$('#getVitalsForm').hide();
			}

			$('#showVitalsForm').addClass('btn btn-default');
		    $('#showVitalsForm').show();
	        
         });
	});

	function getPattern(schedule){

		var days = '';

		days += 'Days: ';
		if (schedule.sunday){
			days += ' Sunday';
		}
		if (schedule.monday){
			days += ' Monday';
		}
		if (schedule.tuesday){
			days += ' Tuesday';
		}
		if (schedule.wednesday){
			days += ' Wednesday';
		}
		if (schedule.thursday){
			days += ' Thursday';
		}
		if (schedule.friday){
			days += ' Friday';
		}
		if (schedule.saturday){
			days += ' Saturday';
		}

		var times = '';
		times = 'In a day: ';
		if (schedule.fasting){
			times += ' Fasting';
		}
		if (schedule.postBreak){
			times += ' Post-Breakfast';
		}
		if (schedule.preLunch){
			times += ' Pre-Lunch';
		}
		if (schedule.postLunch){
			times += ' Post-Lunch';
		}
		if (schedule.preDinner){
			times += ' Pre-Dinner';
		}
		if (schedule.postDinner){
			times += ' Post-Dinner';
		}

		return '<p>' + times + '</p><p>' + days + '</p>';
	}

	$('#showVitalsForm').on("click",function(){

		$('#vitalsForm').show();
		$('#getVitalsForm').hide();
		$('#showVitalsForm').hide();
	});
	</script>
	
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {});

    </script>

</div>
</body>
</html>