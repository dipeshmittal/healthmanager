<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">

    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="/css/plugins/dataTables.bootstrap.css" rel="stylesheet">

</head>
<body>
<div>
	<%@ include file="include/header.jsp" %> 
    <!--BEGIN BACK TO TOP-->
    <a id="totop" href="#"><i class="fa fa-angle-up"></i></a><!--END BACK TO TOP--><!--BEGIN TOPBAR-->
    <div id="header-topbar-option-demo" class="page-header-topbar">
        <%@ include file="include/navigation.html" %> 
    </div>
    <!--END TOPBAR-->
    <div id="wrapper">
    <!--BEGIN SIDEBAR MENU-->
        <nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
		   <div class="sidebar-collapse menu-scroll">
		       <ul id="side-menu" class="nav">
		           <li class="user-panel">
		               <div class="thumb"><img src="/images/ujjwal.jpg" alt="" class="img-circle"/></div>
		               <div class="info"><p>Ujjwal</p>
		                   <ul class="list-inline list-unstyled">
		                       <li><a href="#" data-hover="tooltip" title="Profile"><i class="fa fa-user"></i></a></li>
		                       <li><a href="#" data-hover="tooltip" title="Mail"><i class="fa fa-envelope"></i></a></li>
		                       <li><a href="#" data-hover="tooltip" title="Setting" data-toggle="modal" data-target="#modal-config"><i class="fa fa-cog"></i></a></li>
		                       <li><a href="#" data-hover="tooltip" title="Logout"><i class="fa fa-sign-out"></i></a></li>
		                   </ul>
		               </div>
		               <div class="clearfix"></div>
		           </li>
		           <li><a href="/educator"><i class="fa fa-tachometer fa-fw">
		               <div class="icon-bg bg-orange"></div>
		           </i><span class="menu-title">New Readings</span></a></li>
		           
		
		           <li class="active"><a href="#"><i class="fa fa-th-list fa-fw">
		               <div class="icon-bg bg-blue"></div>
		           </i><span class="menu-title">Create Data</span><span class="fa arrow"></span></a>
		               <ul class="nav nav-second-level">
		                   <li class="active"><a href="/customuser/create"><i class="fa fa-th-large"></i><span class="submenu-title">Create New User</span></a></li>
		               </ul>
		               <ul class="nav nav-second-level">
		                   <li><a href="/customuser/upload"><i class="fa fa-th-large"></i><span class="submenu-title">Upload files for User</span></a></li>
		               </ul>
		           </li>
		           
		           
		           <li><a  href="#"><i class="fa fa-file-o fa-fw">
		               <div class="icon-bg bg-primary"></div>
		           </i><span class="menu-title">View/Edit User Details</span><span class="fa arrow"></span></a>
		           	<ul class="nav nav-second-level">
		                   <li><a href="/customuser/find"><i class="fa fa-user"></i><span class="submenu-title">View details</span></a></li>
		                   <li><a href="/customuser/addGlucoData"><i class="fa fa-lock"></i><span class="submenu-title">Add Gluco Reading</span></a></li>
		               </ul>
		           </li>
		           
		           <li><a href="#"><i class="fa fa-file-o fa-fw">
		               <div class="icon-bg bg-primary"></div>
		           </i><span class="menu-title">Plan Patient activities</span><span class="fa arrow"></span></a>
		           	<ul class="nav nav-second-level">
		                   <li><a href="/customuser/addVitalsSchedule"><i class="fa fa-user"></i><span class="submenu-title">Schedule vitals readings</span></a></li>
		               </ul>
		           </li>
		
		           </ul>
		    </div>
		</nav>

        <!--END CHAT FORM--><!--BEGIN PAGE WRAPPER-->
        <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">Add a new User</div>
                </div>
                <div class="btn btn-blue reportrange"><i class="fa fa-calendar"></i>&nbsp;<span></span>&nbsp;report&nbsp;<i class="fa fa-angle-down"></i><input type="hidden" name="datestart"/><input type="hidden" name="endstart"/></div>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
            <div class="page-content">
                    <div class="row">
						<div class="col-lg-6">
                          <form id="user" role="form" method="post" action="forms/newUser">
                              <div class="form-group">
                                  <label>Email Address</label>
                                  <input class="form-control" name="email" type="email" autofocus>
                              </div>
                              <div class="form-group">
                              	<label>Password</label>
                            <input class="form-control" name="password" type="password">
                        	</div>
                              <div class="form-group">
                                  <label>First Name</label>
                                  <input class="form-control" placeholder="eg: Ujjwal" name="firstName">
                              </div>
                              <div class="form-group">
                                  <label>Last Name</label>
                                  <input class="form-control" placeholder="eg: Chaudhry" name="lastName">
                              </div>
                              <div class="form-group">
								 <label>Date of birth</label>
								 <input type="date" class="form-control" name="dateOfBirth">
							</div>
							<div class="form-group">
                                  <label>Phone Number</label>
                                  <input class="form-control" name="phoneNumber">
                              </div>
                              <div class="form-group">
                                  <label>Alternate Phone Number</label>
                                  <input class="form-control" name="alternatePhoneNumber">
                              </div>
                              <div class="form-group">
                             		<label>Address (Street & flat)</label>
                                  <input class="form-control" name="addressStreet">
                              </div>
                              <div class="form-group">
                             		<label>City</label>
                                  <input class="form-control" name="addressCity">
                              </div>
                              <div class="form-group">
                             		<label>Pincode</label>
                                  <input class="form-control" name="addressPincode">
                              </div>
                              <div class="form-group">
                                  <label>Height (in cms.)</label>
                                  <input class="form-control" name="height">
                              </div>
                              <div class="form-group">
                                  <label>Weight (in kg)</label>
                                  <input class="form-control" name="weight">
                              </div>
                              <div class="form-group">
                                  <label>Gender</label>
                                  <select class="form-control" name="gender">
                                      <option>Male</option>
                                      <option>Female</option>
                                      <option>Others</option>
                                  </select>
                              </div>
                              <div class="form-group">
                                  <label>Blood Group</label>
                                  <select class="form-control" name="bloodGroup">
                                      <option>A+</option>
                                      <option>A-</option>
                                      <option>B+</option>
                                      <option>B-</option>
                                      <option>AB+</option>
                                      <option>AB-</option>
                                      <option>O+</option>
                                      <option>O-</option>
                                  </select>
                              </div>
                              <div class="form-group">
                                  <label>Profession</label>
                                  <input class="form-control" placeholder="eg: Lawyer, Engineer" name="occupation">
                              </div>
                              <button type="submit" class="btn btn-default">Submit Button</button>
                              <button type="reset" class="btn btn-default">Reset Button</button>
                          </form>
                      </div>
                    </div>
                
            </div>
            </div>
        <!--BEGIN FOOTER-->
        <div id="footer">
            <div class="copyright">2014 © &mu;Admin - Responsive Multi-Style Admin Template</div>
        </div>
        <!--END FOOTER--><!--END PAGE WRAPPER--></div>
        <%@ include file="include/footer.jsp" %> 

    <script src="/js/jquery-1.11.0.js"></script>
    <script src="/js/underscore-min.js"></script>
    <script type="text/javascript" src="/js/json2.js"></script>

    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.0.0/backbone-min.js"></script>
    <script type="text/javascript" src="https://rawgithub.com/powmedia/backbone.bootstrap-modal/master/src/backbone.bootstrap-modal.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="/js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {});

    </script>

</div>
</body>
</html>