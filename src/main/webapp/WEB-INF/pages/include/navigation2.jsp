<nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
   <div class="sidebar-collapse menu-scroll">
       <ul id="side-menu" class="nav">
           <li class="user-panel">
               <div class="thumb"><img src="/images/ujjwal.jpg" alt="" class="img-circle"/></div>
               <div class="info"><p>Ujjwal</p>
                   <ul class="list-inline list-unstyled">
                       <li><a href="#" data-hover="tooltip" title="Profile"><i class="fa fa-user"></i></a></li>
                       <li><a href="#" data-hover="tooltip" title="Mail"><i class="fa fa-envelope"></i></a></li>
                       <li><a href="#" data-hover="tooltip" title="Setting" data-toggle="modal" data-target="#modal-config"><i class="fa fa-cog"></i></a></li>
                       <li><a href="#" data-hover="tooltip" title="Logout"><i class="fa fa-sign-out"></i></a></li>
                   </ul>
               </div>
               <div class="clearfix"></div>
           </li>
           <li class="active"><a href="/educator"><i class="fa fa-tachometer fa-fw">
               <div class="icon-bg bg-orange"></div>
           </i><span class="menu-title">New Readings</span></a></li>
           

           <li><a href="#"><i class="fa fa-th-list fa-fw">
               <div class="icon-bg bg-blue"></div>
           </i><span class="menu-title">Create Data</span><span class="fa arrow"></span></a>
               <ul class="nav nav-second-level">
                   <li><a href="/customuser/create"><i class="fa fa-th-large"></i><span class="submenu-title">Create New User</span></a></li>
               </ul>
               <ul class="nav nav-second-level">
                   <li><a href="/customuser/upload"><i class="fa fa-th-large"></i><span class="submenu-title">Upload files for User</span></a></li>
               </ul>
           </li>
           
           
           <li><a  href="#"><i class="fa fa-file-o fa-fw">
               <div class="icon-bg bg-primary"></div>
           </i><span class="menu-title">View/Edit User Details</span><span class="fa arrow"></span></a>
           	<ul class="nav nav-second-level">
                   <li><a href="/customuser/find"><i class="fa fa-user"></i><span class="submenu-title">View details</span></a></li>
                   <li><a href="/customuser/addGlucoData"><i class="fa fa-lock"></i><span class="submenu-title">Add Gluco Reading</span></a></li>
               </ul>
           </li>

           </ul>
    </div>
</nav>