<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">

    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="/css/plugins/dataTables.bootstrap.css" rel="stylesheet">

</head>
<body>
<div>
	<%@ include file="include/header.jsp" %> 
    <!--BEGIN BACK TO TOP-->
    <a id="totop" href="#"><i class="fa fa-angle-up"></i></a><!--END BACK TO TOP--><!--BEGIN TOPBAR-->
    <div id="header-topbar-option-demo" class="page-header-topbar">
        <%@ include file="include/navigation.html" %> 
        <div id="modal-config" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                        <h4 class="modal-title">Modal title</h4></div>
                    <div class="modal-body"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eleifend et nisl eget porta. Curabitur elementum sem molestie nisl varius, eget tempus odio molestie. Nunc vehicula sem arcu, eu pulvinar neque cursus ac. Aliquam ultricies lobortis magna et aliquam. Vestibulum egestas eu urna sed ultricies. Nullam pulvinar dolor vitae quam dictum
                        condimentum. Integer a sodales elit, eu pulvinar leo. Nunc nec aliquam nisi, a mollis neque. Ut vel felis quis tellus hendrerit placerat. Vivamus vel nisl non magna feugiat dignissim sed ut nibh. Nulla elementum, est a pretium hendrerit, arcu risus luctus augue, mattis aliquet orci ligula eget massa. Sed ut ultricies felis.</p></div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!--END MODAL CONFIG PORTLET--></div>
    <!--END TOPBAR-->
    <div id="wrapper">
    <!--BEGIN SIDEBAR MENU-->
        <nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
		   <div class="sidebar-collapse menu-scroll">
		       <ul id="side-menu" class="nav">
		           <li class="user-panel">
		               <div class="thumb"><img src="/images/ujjwal.jpg" alt="" class="img-circle"/></div>
		               <div class="info"><p>Ujjwal</p>
		                   <ul class="list-inline list-unstyled">
		                       <li><a href="#" data-hover="tooltip" title="Profile"><i class="fa fa-user"></i></a></li>
		                       <li><a href="#" data-hover="tooltip" title="Mail"><i class="fa fa-envelope"></i></a></li>
		                       <li><a href="#" data-hover="tooltip" title="Setting" data-toggle="modal" data-target="#modal-config"><i class="fa fa-cog"></i></a></li>
		                       <li><a href="#" data-hover="tooltip" title="Logout"><i class="fa fa-sign-out"></i></a></li>
		                   </ul>
		               </div>
		               <div class="clearfix"></div>
		           </li>
		           <li class="active"><a href="/educator"><i class="fa fa-tachometer fa-fw">
		               <div class="icon-bg bg-orange"></div>
		           </i><span class="menu-title">New Readings</span></a></li>
		           
		
		           <li><a href="#"><i class="fa fa-th-list fa-fw">
		               <div class="icon-bg bg-blue"></div>
		           </i><span class="menu-title">Create Data</span><span class="fa arrow"></span></a>
		               <ul class="nav nav-second-level">
		                   <li><a href="/customuser/create"><i class="fa fa-th-large"></i><span class="submenu-title">Create New User</span></a></li>
		               </ul>
		               <ul class="nav nav-second-level">
		                   <li><a href="/customuser/upload"><i class="fa fa-th-large"></i><span class="submenu-title">Upload files for User</span></a></li>
		               </ul>
		           </li>
		           
		           
		           <li><a href="#"><i class="fa fa-file-o fa-fw">
		               <div class="icon-bg bg-primary"></div>
		           </i><span class="menu-title">View/Edit User Details</span><span class="fa arrow"></span></a>
		           	<ul class="nav nav-second-level">
		                   <li><a href="/customuser/find"><i class="fa fa-user"></i><span class="submenu-title">View details</span></a></li>
		                   <li><a href="/customuser/addGlucoData"><i class="fa fa-lock"></i><span class="submenu-title">Add Gluco Reading</span></a></li>
		               </ul>
		           </li>
		           
		           <li><a href="#"><i class="fa fa-file-o fa-fw">
		               <div class="icon-bg bg-primary"></div>
		           </i><span class="menu-title">Plan Patient activities</span><span class="fa arrow"></span></a>
		           	<ul class="nav nav-second-level">
		                   <li><a href="/customuser/addVitalsSchedule"><i class="fa fa-user"></i><span class="submenu-title">Schedule vitals readings</span></a></li>
		                   <li><a href="/customuser/addOPDSchedule"><i class="fa fa-user"></i><span class="submenu-title">Schedule OPD visits</span></a></li>
		               </ul>
		           </li>
		
		           </ul>
		    </div>
		</nav>

        <!--END CHAT FORM--><!--BEGIN PAGE WRAPPER-->
        <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">Patient Readings</div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
            <div class="page-content">
                    <div class="row">
                        <div>
                            <ul id="myTab" class="nav nav-tabs" role="tablist">
                              <li class="active"><a href="#new_readings" role="tab" data-toggle="tab">New readings</a></li>
                              <li class=""><a href="#flagged_readings" role="tab" data-toggle="tab">Flagged readings</a></li>
                              <li class=""><a href="#reviewed_readings" role="tab" data-toggle="tab">Reveiwed readings</a></li>
                            </ul>

                            <div id="myTabContent" class="tab-content">
                              <div class="tab-pane fade active in" id="new_readings">
                                <div class="panel panel-yellow">
                                    <div class="panel-heading">
                                        Patient readings pending comments:
                                    </div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTable" id="dataTables-example1">
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.panel -->
                              </div>
                              <div class="tab-pane fade" id="flagged_readings">
                                <div class="panel panel-yellow">
                                    <div class="panel-heading">
                                        Flagged patient readings pending comments:
                                    </div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTable" id="dataTables-example2">
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.panel -->
                              </div>
                              <div class="tab-pane fade" id="reviewed_readings">
                                <div class="panel panel-yellow">
                                    <div class="panel-heading">
                                        Reviewed Patient readings with comments:
                                    </div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTable" id="dataTables-example3">
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.panel -->
                              </div>
                            </div>
                        </div>
                    </div>
                
            </div>
            
        <!--BEGIN FOOTER-->
        <div id="footer">
            <div class="copyright">2014 © &mu;Admin - Responsive Multi-Style Admin Template</div>
        </div>
        <!--END FOOTER--><!--END PAGE WRAPPER--></div>
        <%@ include file="include/footer.jsp" %> 

    <script src="/js/jquery-1.11.0.js"></script>
    <script src="/js/underscore-min.js"></script>
    <script type="text/javascript" src="/js/json2.js"></script>

    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.0.0/backbone-min.js"></script>
    <script type="text/javascript" src="https://rawgithub.com/powmedia/backbone.bootstrap-modal/master/src/backbone.bootstrap-modal.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="/js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {

        // Fetch data from server for the glucose readings
        $.get('/fetchGlucoseData/', function (json_response){
            
            var new_readings = JSON.parse(json_response).new_readings;
            if (new_readings.length > 0){
                var str = '';
                str += "<tbody>";
                for (var i = 0; i < new_readings.length; i++){
                    str += "<tr class=" + ((i+1) % 2 ? 'odd gradeX' : 'even gradeC') + ">"+
                                "<td>" + new_readings[i].uid + "</td>"+
                                "<td>" + new_readings[i].first_name + " " + new_readings[i].last_name + "</td>" +
                                "<td>" + new_readings[i].time_taken+ "</td>"+
                                "<td><a href='#' data-hover='tooltip' onclick='openModal(" + JSON.stringify(new_readings[i]) + ")' data-toggle='modal'>" + new_readings[i].value + "</a></td>" +
                                "<td>" + convertType(new_readings[i].reading_type) + "</td>"+
                                "</tr>";
                }
                str += "</tbody>";

                var tableHead = "<thead><tr><th>Kronica UID</th><th>Name</th><th>Time of reading</th><th>Reading</th><th>Type</th></tr></thead>";

                document.getElementById('dataTables-example1').innerHTML = tableHead + str;
            }
            else {
                document.getElementById('dataTables-example1').innerHTML = "No New patient data as of now.";
            }

            var flagged_readings = JSON.parse(json_response).flagged_readings;
            if (flagged_readings.length > 0){
                var str = '';
                str += "<tbody>";
                for (var i = 0; i < flagged_readings.length; i++){
                    str += "<tr class=" + ((i+1) % 2 ? 'odd gradeX' : 'even gradeC') + ">"+
                                "<td>" + flagged_readings[i].uid + "</td>"+
                                "<td>" + flagged_readings[i].first_name + " " + flagged_readings[i].last_name + "</td>" +
                                "<td>" + flagged_readings[i].time_taken+ "</td>"+
                                "<td>" + flagged_readings[i].comment+ "</td>"+
                                "<td><a href='#' data-hover='tooltip' onclick='openModal(" + JSON.stringify(flagged_readings[i]) + ")' data-toggle='modal'>" + flagged_readings[i].value + "</a></td>" +
                                "<td>" + convertType(flagged_readings[i].reading_type) + "</td>"+
                                "</tr>";
                }
                str += "</tbody>";

                var tableHead = "<thead><tr><th>Kronica UID</th><th>Name</th><th>Time of reading</th><th>Reason for flagging</th><th>Reading</th><th>Type</th></tr></thead>";

                document.getElementById('dataTables-example2').innerHTML = tableHead + str;
            }
            else {
                document.getElementById('dataTables-example2').innerHTML = "No flagged patient data as of now.";
            }

            var reviewed_readings = JSON.parse(json_response).reviewed_readings;
            if (reviewed_readings.length > 0){
                var str = '';
                str += "<tbody>";
                for (var i = 0; i < reviewed_readings.length; i++){
                    str += "<tr class=" + ((i+1) % 2 ? 'odd gradeX' : 'even gradeC') + ">"+
                                "<td>" + reviewed_readings[i].uid + "</td>"+
                                "<td>" + reviewed_readings[i].first_name + " " + reviewed_readings[i].last_name + "</td>" +
                                "<td>" + reviewed_readings[i].time_taken+ "</td>"+
                                "<td><a href='#' data-hover='tooltip' onclick='openModal(" + JSON.stringify(reviewed_readings[i]) + ")' data-toggle='modal'>" + reviewed_readings[i].value + "</a></td>" +
                                "<td>" + convertType(reviewed_readings[i].reading_type) + "</td>"+
                                "<td>" + reviewed_readings[i].comment+ "</td>"+
                                "</tr>";
                }
                str += "</tbody>";

                var tableHead = "<thead><tr><th>Kronica UID</th><th>Name</th><th>Time of reading</th><th>Reading</th><th>Type</th><th>Comments</th></tr></thead>";

                document.getElementById('dataTables-example3').innerHTML = tableHead + str;
            }
            else {
                document.getElementById('dataTables-example3').innerHTML = "No flagged patient data as of now.";
            }

        });

        $('#dataTables-example1').dataTable({
            "lengthMenu": [ 5, 10, 15 ]
        });
        $('#dataTables-example2').dataTable({
            "lengthMenu": [ 5, 10, 15 ]
        });
        $('#dataTables-example3').dataTable({
            "lengthMenu": [ 5, 10, 15 ]
        });

        $("#commentButton").click(function(){
          $.post("/addComment/",
          {
            id: 3,
            commentText: $('#commentText').getText,
            type: 'F'
          },
          function(data,status){
            alert("Data: " + data + "\nStatus: " + status);
          });
        });

        $(document).on("click", ".open-AddCommentDialog", function(){
           console.log('yahoo');
           var uid = $(this).data('uid');
           $("#userName").text( 'Readings from ' + uid );
        });

    });

    function openModal(str){

        var read = eval(str);
        console.log("/fetchHistory/?uid=" + read.uid + '&type=' + read.reading_type);

        $.get("/fetchHistory/?uid=" + read.uid + '&type=' + read.reading_type, function(data,status){

            var readings = Backbone.Model.extend(data);

            console.log('In openmodal ' + str);
            data = JSON.parse(data);
            data.current_reading = str;
            var view = new ModalView({model: readings, data: data});

            var modal = new Backbone.BootstrapModal({
                content: view,
                title: 'Add comment for ' + read.first_name + ' ' + read.last_name,
                animate: true
            });
            modal.open(function(){
                console.log('Modal opened');
				console.log('Modal : ' + modal);
            });
        });
    }

    var ModalView = Backbone.View.extend({
        tagName: 'h1',
        template: $('#comment-model-id'),
        initialize: function(options) {
            this.options = options;
            _.bindAll(this, 'render');
        },
        render: function() {

            var new_readings = this.options.data.new_readings;
            var current_reading_id = this.options.data.current_reading.id;
            
			var row = document.createElement('div');
			$(row).addClass('row');
			$(row).css('font-size', '14px');

			var col = document.createElement('div');
			$(col).addClass('col-lg-5');

			var h5 = document.createElement('h5');
			h5.appendChild(document.createTextNode('Past 5 Readings'));
			var table_responsive = document.createElement('div');
			$(table_responsive).addClass('table_responsive');

            var tbody = document.createElement('tbody');
            
            var thead = document.createElement('thead'); 
            var t = document.createElement('tr');
            var th1 = document.createElement('th');
            th1.appendChild(document.createTextNode('Time taken'));

            var th2 = document.createElement('th');
            th2.appendChild(document.createTextNode('Value'));

            t.appendChild(th1);
            t.appendChild(th2);
            thead.appendChild(t);
            
            for (var i = 0; i < new_readings.length; i++){
                var tr = document.createElement('tr');   
                $(tr).addClass( (i+1) % 2 ? 'odd gradeX' : 'even gradeC');

                var td1 = document.createElement('td');
                var td2 = document.createElement('td');

                var text1 = document.createTextNode(new_readings[i].time_taken);
                var text2 = document.createTextNode(new_readings[i].value);

                td1.appendChild(text1);
                td2.appendChild(text2);
                tr.appendChild(td1);
                tr.appendChild(td2);

                tbody.appendChild(tr);
            }

            var table = document.createElement('table');
            $(table).addClass('table table-striped table-bordered table-hover');
            table.appendChild(thead);
            table.appendChild(tbody);

            table_responsive.appendChild(table);

            col.appendChild(h5);
            col.appendChild(table_responsive);

            row.appendChild(col);

            var col2 = document.createElement('div');
			$(col2).addClass('col-lg-7');

			var h5_1 = document.createElement('h5');
			h5_1.appendChild(document.createTextNode('Actions'));

			var div3 = document.createElement('div');

			var but1 = document.createElement('button');
			$(but1).addClass('btn btn-primary');
			$(but1).click(function() {
				var ele = document.createElement('textarea');     
		        $(ele).css({margin : '0px', height: '225px', width: '300px'});
				$(ele).attr("placeholder","Add comments for the patient...");

				var okBut = document.createElement('button');
				$(okBut).addClass('btn ok btn-primary');
				okBut.appendChild(document.createTextNode('Ok'));

				$(okBut).click(function() {
					console.log($(ele).val());	
					var form = document.createElement("form");
				    form.setAttribute("method", "POST");
				    form.setAttribute("action", "addCommentForReading");
				    form.setAttribute("id", "readingComment");

				    var params = {id : current_reading_id, 
						  	 comment : $(ele).val()};
				  	 
				    for(var key in params) {
				        if(params.hasOwnProperty(key)) {
				            var hiddenField = document.createElement("input");
				            hiddenField.setAttribute("type", "hidden");
				            hiddenField.setAttribute("name", key);
				            hiddenField.setAttribute("value", params[key]);

				            form.appendChild(hiddenField);
				         }
				    }

				    form.submit();
				    
				});
				
				$(div3).html(ele);
				div3.appendChild(okBut);		        

			});
			but1.appendChild(document.createTextNode('Add comment'));

			var h4 = document.createTextNode('OR');

			var but2 = document.createElement('button');
			$(but2).addClass('btn btn-primary');
			$(but2).click(function() {

				var ele = document.createElement('textarea');     
		        $(ele).css({margin : '0px', height: '225px', width: '300px'});
				$(ele).attr("placeholder","Add comments for the patient...");

				var okBut = document.createElement('button');
				$(okBut).addClass('btn ok btn-primary');
				okBut.appendChild(document.createTextNode('Ok'));

				$(okBut).click(function() {
					console.log($(ele).val());	
					var form = document.createElement("form");
				    form.setAttribute("method", "POST");
				    form.setAttribute("action", "addFlagForReading");
				    form.setAttribute("id", "readingComment");

				    var params = {id : current_reading_id, 
						  	 comment : $(ele).val()};
				  	 
				    for(var key in params) {
				        if(params.hasOwnProperty(key)) {
				            var hiddenField = document.createElement("input");
				            hiddenField.setAttribute("type", "hidden");
				            hiddenField.setAttribute("name", key);
				            hiddenField.setAttribute("value", params[key]);

				            form.appendChild(hiddenField);
				         }
				    }

				    form.submit();
				    
				});
				
				$(div3).html(ele);
				div3.appendChild(okBut);					  
			});
			but2.appendChild(document.createTextNode('Flag'));

			div3.appendChild(but1);
			div3.appendChild(document.createElement('br'));
			div3.appendChild(document.createElement('br'));
			div3.appendChild(h4);
			div3.appendChild(document.createElement('br'));
			div3.appendChild(document.createElement('br'));
			div3.appendChild(but2);
			
			col2.appendChild(h5_1);
			col2.appendChild(div3);
			
			row.appendChild(col2);
			
            this.$el.html(row);
            return this;
        }
    });

    function openTextBox(type){

        if (type === 'comment'){
            return "<form role='form' method='post' action='addComment'>" +
            		"<div class='form-group'>" +
            			"<textarea rows='3' cols='25' placeholder='Add comments for the patient...'></textarea>" +
				    "</div>" +
				    "<div class='form-group'>" +
				    	"<input hidden='true' name='currentReadingId' value='" + this.options.data.current_reading_id + "'>" +
				    "</div>" +
				    "<button type='submit' class='btn ok btn-primary'>Continue</button>" +
				    "</form>";
        }
        else if (type === 'flag'){
            return "<textarea id='flagText' style='margin: 0px; height: 224px; width: 300px;' placeholder='Add comments to review later...'></textarea>" + 
   		 			"<br><button class='btn ok btn-primary'>Ok</button";
        }

    }

    function convertType(type){
		if (type){

			if (type === "0"){
				return "Fasting";
			}
			else if (type === "1"){
				return "Post-Breakfast";
			}
			else if (type === "2"){
				return "Pre-Lunch";
			}
			else if (type === "3"){
				return "Post-Lunch";
			}
			else if (type === "4"){
				return "Pre-Dinner";
			}
			else if (type === "5"){
				return "Post-Dinner";
			}
			else if (type === "7"){
				return "Post-Medication/Insulin";
			}
			else if (type === "7"){
				return "Random";
			}
		}

		return null;
    }
    
    </script>

</div>
</body>
</html>